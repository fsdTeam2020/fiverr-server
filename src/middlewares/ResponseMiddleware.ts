import { NextFunction, Request, Response } from "express";
import moment = require("moment-timezone");
import { C } from "../config/constants/constants";
import { Utils } from "../lib/Utils";
moment.tz.setDefault(C.TIMEZONE);
import { MessagesConstants as Msg } from "../config/constants/messages.constants";
import { ResponseFormatter } from "../lib/ResponseFormatter";
import { ApiLogMiddleware } from "./ApiLogMiddleware";

export class ResponseMiddleware {

    public static sendResponse(response: { isSuccess: boolean, data: any }, req: Request, res: Response, next: NextFunction) {

        if (response.isSuccess) {
            ResponseMiddleware.success(req, res, response.data);
        } else {
            ResponseMiddleware.fail(req, res, response.data);
        }
    }

    public static success(req: Request, res: Response, data: any, sendRaw: boolean = false) {

        let httpStatusCode: number;

        try {
            const newParams = { response: data };
            ApiLogMiddleware.logApiResponse(req, res, newParams);
            httpStatusCode = 200;

        } catch (err) {
            const errResponse: any = ResponseFormatter.getErrorResponseWithBody(500, "RF001", Msg.SOMETHING_WENT_WRONG);
            data = errResponse.body;
            httpStatusCode = errResponse.status;
        }

        console.log("Response Timestamp:", moment().format());

        if (sendRaw) {
            return res.status(httpStatusCode).send(data);
        } else {
            res.status(httpStatusCode).send({ success: data });
        }

    }

    public static fail(req: Request, res: Response, error: any, refId?: string) {

        let data: any;
        let status: number;

        try {
            let loggedError = {};

            if (!Utils.isEmpty(error.loggedError)) {
                loggedError = error.loggedError;
                delete error.loggedError;
            }

            const newParams = { response: error, loggedError, refId };
            res.__responseData = newParams;
            ApiLogMiddleware.logApiResponse(req, res, newParams);

            if (!Utils.isEmpty(error.status)) {
                data = error.body;
                status = error.status;
            } else {
                const errResponse: any = ResponseFormatter.getErrorResponseWithBody(500, "RF002", Msg.SOMETHING_WENT_WRONG);
                data = errResponse.body;
                status = errResponse.status;
            }
        } catch (err) {
            const errResponse: any = ResponseFormatter.getErrorResponseWithBody(500, "RF003", Msg.SOMETHING_WENT_WRONG);
            data = errResponse.body;
            status = errResponse.status;
        }

        console.log("Response Timestamp:", moment().format());
        res.status(status).send({ errors: data });
    }

    constructor() {

    }
}
