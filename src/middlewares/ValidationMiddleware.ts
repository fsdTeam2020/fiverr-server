import { NextFunction, Request, Response } from "express";
import Joi from "joi";

import { UserValidationSchema } from "../core/validations/schemas/UserValidationSchema";
import { EncryptionValidationSchema } from "../core/validations/schemas/EncryptionValidationSchema";
import { get, has, merge } from "lodash";

export class ValidationMiddleware {

    public static loadSchemas() {

        return merge(
            ValidationMiddleware.schemas,
            UserValidationSchema.schemas,
            EncryptionValidationSchema.schemas,
        );
    }

    public static async validate(req: Request, res: Response, next: NextFunction) {

        const method = req.method.toLowerCase();
        const route = req.route.path;
        const schemas = ValidationMiddleware.loadSchemas();

        if (has(schemas, route)) {

            const schema = get(schemas, route);
            try {

                const data = await Joi.validate(req.body, schema);
                req.body = data;
                next();
            } catch (error) {

                console.log(error);
                next(error);
            }
        }
        next();
    }

    private static schemas: object = {};
    private static methods = {

        get: "params",
        post: "body",
        put: "body",
    };

    constructor() {

    }
}
