import { NextFunction, Request, Response } from "express";
import moment = require("moment-timezone");
moment.tz.setDefault("Asia/Calcutta");

import { ApiLogService } from "../common/services/ApiLogService";

export class ApiLogMiddleware {

    public static async logApiRequest(req: Request, res: Response, next: NextFunction) {

        console.log("Request Timestamp:", moment().format(), `${req.method} ${req.path} ${req.ip}`);

        const apiLogService: ApiLogService = new ApiLogService();
        const logData: any = await apiLogService.createApiLog(req, res);
        req.__apiLogRequestId = logData._id;
        next();
    }

    public static async logApiResponse(req: Request, res: Response, params: any) {

        const apiLogService: ApiLogService = new ApiLogService();
        await apiLogService.updateApiLog(req, res, req.__apiLogRequestId, params);
    }

    constructor() {

    }
}
