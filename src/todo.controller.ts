import { BodyProp, Controller, Delete, Get, Post, Put, Route } from "tsoa";
import { ITodo, TodoModel } from "./todo";

@Route("/todo")
export class TodoController extends Controller {
    @Get()
    public async getAll(): Promise<ITodo[]> {
        let items: any = await TodoModel.find({});
        try {
            items = items.map((item: any) => ({ id: item._id, description: item.description }));
        } catch (err) {
            this.setStatus(500);
            console.error("Caught error", err);
        }
        return items;
    }

    @Post()
    public async create(@BodyProp() description: string): Promise<void> {
        const item = new TodoModel({ description });
        await item.save();
    }

    @Put("/{id}")
    public async update(id: string, @BodyProp() description: string): Promise<void> {
        await TodoModel.findOneAndUpdate({ _id: id }, { description });
    }

    @Delete("/{id}")
    public async remove(id: string): Promise<void> {
        await TodoModel.findByIdAndRemove(id);
    }
}
