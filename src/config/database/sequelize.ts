import * as fs from "fs";
import * as path from "path";
import { Sequelize } from "sequelize";
import { Utils } from "../../lib/Utils";

import { C } from "../constants/constants";

// export const database = new Sequelize({
//     database: "payment_middleware",
//     dialect: "mysql",
//     storage: ":memory:",
// });

export class MysqlDB {

    public sequelizeCon: Sequelize;
    constructor() {

        this.sequelizeCon = this.bootstrap();
    }

    private bootstrap(): Sequelize {

        const connectionUri: string = this.getDBConnection();

        const conObj = new Sequelize(connectionUri, {
            timezone: "+05:30",
            dialect: "mysql",
            // storage: config.db.storage,
            // protocol: "tcp",
            define: {

                underscored: true,
                freezeTableName: false,
                charset: "utf8",

                // The `timestamps` field specify whether or not the `createdAt` and `updatedAt` fields will be created.
                // This was true by default, but now is false by default
                timestamps: true,
            },
            dialectOptions: {
                // collate: "utf8_general_ci",
            },
            logging: true,
            omitNull: true, // (default: false, disable inserting undefined values as NULL)
            /**
             * pool: {
             *     max: 5, (default, Maximum number of connection in pool)
             *     min: 0, (default, Minimum number of connection in pool)
             *     acquire: 30000 (default, The maximum time, in milliseconds, that pool will try to get connection before throwing error),
             *     idle: 10000, (default, The maximum time, in milliseconds, that a connection can be idle before being released.)
             *     evict: 1000 (default, The time interval, in milliseconds, after which sequelize-pool will remove idle connections.)
             *   }
             */
        });
        conObj
            .authenticate()
            .then(() => {
                console.log("Connection has been established successfully.");
            })
            .catch((err: any) => {
                console.error("Unable to connect to the database:", err);
                process.exit(1);
            });

        // if (process.env.NODE_ENV === "development" && C.isForceRecreateTable) {
            // conObj.query("SET FOREIGN_KEY_CHECKS = 0")
            //     .then(() => {
            //         return conObj.sync({ force: C.isForceRecreateTable });
            //     })
            //     .then(() => {
            //         return conObj.query("SET FOREIGN_KEY_CHECKS = 1");
            //     })
            //     .then(() => {
            //         console.log("Database synchronised.");
            //     }, (err) => {
            //         console.log(err);
            //     });
        // }

        // const db: any = {};
        // const modelPath = path.join(__dirname, "../../models/mysql");
        // fs
        //     .readdirSync(modelPath)
        //     .filter((file) => (file.indexOf(".") !== 0) && (file.slice(-3) === ".ts"))
        //     .forEach((file) => {
        //         const model = conObj.import(modelPath + file);
        //         db[model.name] = model;
        //     });

        // Object.keys(db).forEach((modelName) => {
        //     if (db[modelName].associate) {
        //         db[modelName].associate(db);
        //     }
        // });
        // db.sequelize = conObj;
        // return db;
        return conObj;
    }

    // sequelize.close() to close connection

    private getDBConnection(): string {

        let connectionString = "";
        if (Utils.isEmpty(C.mySqlDbConfig.password)) {
            connectionString = `mysql://${C.mySqlDbConfig.username}@${C.mySqlDbConfig.host}/${C.mySqlDbConfig.dbName}`;
        } else {
            connectionString = `mysql://${C.mySqlDbConfig.username}:${C.mySqlDbConfig.password}@${C.mySqlDbConfig.host}/${C.mySqlDbConfig.dbName}`;
        }
        console.log(connectionString);
        return connectionString;
    }
}

export const sequelizeCon = new MysqlDB().sequelizeCon;
