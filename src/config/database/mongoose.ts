import mongoose = require("mongoose");
import acl = require("acl");
import { C } from "../constants/constants";

// let winston = require('winston');
export class MongoDB {

    // public mongoDBObj: mongoose.Connection;
    constructor() {

    //     this.bootstrap();
    // }

    // private bootstrap(): void {

        // const mongoDb = mongoose.connection;
        // mongoose.Promise = global.Promise;

        const mongooseOptions = {

            useNewUrlParser: true,
            useFindAndModify: false,
            auto_reconnect: true,
            connectTimeoutMS: 3600000,
            // keepAlive: 120,

            // Disable it for Production, Recommended
            autoIndex: true,

            socketTimeoutMS: 3600000,   // Close sockets after 5 seconds of inactivity
            reconnectTries: 60,        // retry to connect for 60 times at every reconnectInterval
            reconnectInterval: 5000,   // wait 5 second before retrying

            useUnifiedTopology: true,
        };

        const mongoURI: string = this.getDBConnection();

        // mongoose.Promise = global.Promise;

        // use createConnection instead of calling mongoose.connect so we can use multiple connections
        const mongoDb = mongoose.connect(mongoURI, mongooseOptions);
        // tslint:disable-next-line: no-shadowed-variable
        // const mongoDb = mongoose.createConnection(mongoURI, mongooseOptions);
        // this.mongoDBObj = mongoDb;
        mongoose.set("useCreateIndex", true);

        mongoose.connection.on("open", (ref: any) => {
            console.log("info", "Connected to mongo server.");
            const mongoBackend = new acl.mongodbBackend( mongoose.connection.db, "acl_", true);

            // Create a new access control list by providing the mongo backend
            //  Also inject a simple logger to provide meaningful output
            C.aclObj = new acl( mongoBackend, {
                debug( msg: any ) {
                    console.log( "-DEBUG-", msg );
                },
            });
        });

        mongoose.connection.on("error", (err: any) => {
            console.log("error", "Could not connect to mongo server!", err);
        });

        // https://stackoverflow.com/questions/16226472/mongoose-autoreconnect-option
        let mongoDisconnectionsCount = 0;

        mongoose.connection.on("reconnected", () => {
            mongoDisconnectionsCount = 0;
            console.log("info", "MongoDB reconnected!");
        });

        mongoose.connection.on("disconnected", () => {
            console.log("error", "MongoDB disconnected!");
            ++mongoDisconnectionsCount;
        });
    }

    private getDBConnection(): string {
        let mongodbConnectionString = "";
        if (!C.mongoDbConfig.authSource) {

            // mongodbConnectionString = "mongodb://" + (C.mongoDbConfig.host).join() + "/" + C.mongoDbConfig.dbName;
            mongodbConnectionString = "mongodb://" + C.mongoDbConfig.username + ":" +
                C.mongoDbConfig.password + "@" + (C.mongoDbConfig.host).join() + "/" + C.mongoDbConfig.dbName;
        } else {

            mongodbConnectionString = "mongodb://" + C.mongoDbConfig.username + ":" +
                C.mongoDbConfig.password + "@" + (C.mongoDbConfig.host).join() + "/" + C.mongoDbConfig.dbName +
                "?ssl=false&authSource=" + C.mongoDbConfig.authSource;

            if (C.mongoDbConfig.replicaSetName) {
                mongodbConnectionString += "&replicaSet=" + C.mongoDbConfig.replicaSetName;
            }
        }
        console.log(mongodbConnectionString);
        return mongodbConnectionString;
    }

}

// export const mongoDb = new MongoDB();
