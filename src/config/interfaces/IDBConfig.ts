export interface IMongoDbConfig {

    readonly username: string;
    readonly password: string;
    readonly host: string[];
    readonly dbName: string;
    readonly replicaSetName: string;
    readonly authSource: string;
}

export interface IMySqlDbConfig {

    readonly username: string;
    readonly password: string;
    readonly host: string[];
    readonly dbName: string;
}
