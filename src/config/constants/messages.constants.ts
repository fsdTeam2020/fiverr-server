export class MessagesConstants {

    public static readonly BLANK_MSG = "";
    public static readonly LOGOUT = "Logout successfully.";
    public static readonly MAIL_SENDING_FAILED = "Mail sending failed.";
    public static readonly MAIL_SEND = "Mail send successfully.";
    public static readonly MAIL_VERIFIED = "Email id verified successfully.";
    public static readonly NOTIFICATION_SENDING_FAILED = "Notification sending failed.";
    public static readonly NOTIFICATION_SEND = "Notification send successfully.";
    public static readonly OTP_SENDING_FAILED = "OTP sending failed.";
    public static readonly OTP_SEND = "OTP send successfully.";
    public static readonly OTP_VERIFICATION_FAILED = "OTP verification failed.";
    public static readonly OTP_VERIFIED = "OTP verified successfully.";
    public static readonly RECORD_UPDATED_SUCCESSFULLY = "Record updated successfully.";
    public static readonly RECORD_UPDATION_FAILED = "Record updation failed.";
    public static readonly SOMETHING_WENT_WRONG = "Something went wrong.";
    public static readonly AUTHENTICATION_ERROR = "Authentication error. Valid login details required.";
    public static readonly NO_RECORDS_FOUND = "No records found.";
    public static readonly INVALID_REQUEST = "Invalid request.";
    public static readonly USER_TYPE_REQUIRED = "Valid user type required.";
    public static readonly USER_ALREADY_REGISTER = "User already register.";
    public static readonly INVALID_USER_NAME_PASSWORD = "Invalid user-name/password.";
    public static readonly FILE_UPLOADING_FAILED = "File uploading failed.";
    public static readonly FILE_UPLOADED_SUCCESSFULLY = "File uploaded successfully.";
    public static readonly INVALID_USER_PROFILE = "Invalid user profile.";
    public static readonly PERMISSION_ERROR = "User Permissions Could not be fetched";

}
