import * as config from "../config";
import { IMongoDbConfig, IMySqlDbConfig } from "../interfaces/IDBConfig";

// tslint:disable-next-line: no-var-requires
// const EnvConstants = require(`./${ config.NODE_ENV }.constants`).default;

// export default class Constants extends EnvConstants {
class Constants {

    /**
     * Environment Constants
     */
    public readonly ENC_PASS_PHRASE = config.ENC_PASS_PHRASE;
    public readonly TOKEN = config.TOKEN;
    public readonly AUTH_KEY = config.AUTH_KEY;

    /**
     * DB Constants
     */
    public readonly mongoDbConfig: IMongoDbConfig = {

        username: config.DB_MONGO_USERNAME,
        password: config.DB_MONGO_PASSWORD,
        host: config.DB_MONGO_HOST.split(","),
        dbName: config.DB_MONGO_NAME,
        replicaSetName: config.DB_MONGO_REPLICASET_NAME,
        authSource: config.DB_MONGO_AUTH_SOURCE,
    };

    public readonly mySqlDbConfig: IMySqlDbConfig = {
        username: config.DB_MySQL_USERNAME,
        password: config.DB_MySQL_PASSWORD,
        host: config.DB_MySQL_HOST.split(","),
        dbName: config.DB_MySQL_NAME,
    };

    /**
     * Application Constants
     */
    public readonly TIMEZONE: string = "Asia/Calcutta";
    public readonly ENC_ALGORITHM: string = "aes-256-cbc";
    public readonly DATE_FORMAT: string = "YYYY-MM-DD";
    public readonly DATE_TIME_FORMAT: string = "YYYY-MM-DD HH:mm:ss";

    public readonly ERROR_CATEGORY: any = {
        GIBPL: "GIBPL-error",
        INSURER: "Insurer-error",
    };

    public readonly OTP_SOURCES: any = {
        SMS: "SMS",
        MAIL: "Mail",
    };

    public readonly USER_CATEGORY_TYPE: any = {
        BEGINNER: "Beginner",
        INTERMEDIATE: "Intermediate",
        PRO: "Pro",
    };

    public readonly USER_TYPE: any = {
        CUSTOMER: "Customer",
        SELLER: "Seller",
        ADMIN: "Admin",
    };

    public readonly GENDER: any = {
        MALE: "M",
        FEMALE: "F",
        OTHER: "O",
    };

    public aclObj: any = {};
    constructor() {

    }
}

export const C = new Constants();
