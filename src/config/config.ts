import * as dotenv from "dotenv";

dotenv.config();

export const PORT = process.env.PORT || 3008;
export const NODE_ENV = process.env.NODE_ENV || "development";
export const ENC_PASS_PHRASE: string = process.env.NODE_ENV as string;
export const TOKEN = process.env.NODE_ENV as string;
export const DB_MONGO_USERNAME: string = process.env.DB_MONGO_USERNAME as string;
export const DB_MONGO_PASSWORD: string = process.env.DB_MONGO_PASSWORD as string;
export const DB_MONGO_HOST: string = process.env.DB_MONGO_HOST as string;
export const DB_MONGO_NAME: string = process.env.DB_MONGO_NAME as string;
export const DB_MONGO_REPLICASET_NAME: string = process.env.DB_MONGO_REPLICASET_NAME as string;
export const DB_MONGO_AUTH_SOURCE: string = process.env.DB_MONGO_AUTH_SOURCE as string;
export const AUTH_KEY: string = process.env.AUTH_KEY as string;

export const DB_MySQL_USERNAME: string = process.env.DB_MySQL_USERNAME as string;
export const DB_MySQL_PASSWORD: string = process.env.DB_MySQL_PASSWORD as string;
export const DB_MySQL_HOST: string = process.env.DB_MySQL_HOST as string;
export const DB_MySQL_NAME: string = process.env.DB_MySQL_NAME as string;
