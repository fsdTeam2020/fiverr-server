import { Document, model, Schema } from "mongoose";

export interface IApiLog extends Document {

    apiName: string;
    requestUrl: string;
    requestMethod: string;
    requestHeaders: Schema.Types.Mixed;
    getRequestParams: Schema.Types.Mixed;
    postRequestParams: Schema.Types.Mixed;
    response: Schema.Types.Mixed;
    source: string;
    subSource: string;
    requestTime: Date;
    responseTime: Date;
}
