import { Document, model, Schema } from "mongoose";
import { IApiLog } from "../interfaces/IApiLog";

const ApiLogModelSchema: Schema = new Schema(
    {
        apiName: String,
        requestUrl: String,
        requestMethod: String,

        requestHeaders: Schema.Types.Mixed,
        responseHeaders: Schema.Types.Mixed,

        getRequestParams: Schema.Types.Mixed,
        postRequestParams: Schema.Types.Mixed,

        response: Schema.Types.Mixed,
        loggedError: Schema.Types.Mixed,

        source: { type: String, index: true },
        subSource: { type: String, index: true },

        requestTime: { type: Date, default: Date.now },
        responseTime: { type: Date, default: Date.now },
    },
    {
        collection: "api_log", timestamps: true,
    },
);

export const ApiLogModel = model<IApiLog>("ApiLogModel", ApiLogModelSchema);
