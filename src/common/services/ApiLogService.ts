import { Request, Response } from "express";
import moment = require("moment-timezone");
import { C } from "../../config/constants/constants";
moment.tz.setDefault(C.TIMEZONE);

import { IApiLog } from "../interfaces/IApiLog";
import { ApiLogModel } from "../models/ApiLogModel";

export class ApiLogService {

    constructor() {

    }

    public async createApiLog(req: Request, res: Response) {

        const apiData = {
            apiName: req.path,
            requestUrl: req.protocol + "://" + req.get("host") + req.path,
            requestMethod: req.method,
            requestHeaders: req.headers,

            getRequestParams: req.query,
            postRequestParams: req.body,

            response: "",
            requestTime: moment().toDate(),
        };

        const apiLogM = new ApiLogModel(apiData);
        return await apiLogM.save();
    }

    public async updateApiLog(req: Request, res: Response, id: any, params: object) {

        await ApiLogModel.findByIdAndUpdate(id, params);
    }
}
