import { NextFunction, Request, Response, Router } from "express";
import { C } from "../config/constants/constants";
import { CommonController } from "../core/controllers/CommonController";
import { ValidationMiddleware } from "../middlewares/ValidationMiddleware";

export class CommonRoutes {

    public router: Router = Router();
    private common: CommonController;

    constructor(app: Router) {

        this.index(app);
        this.common = new CommonController();
    }

    /**
     * Define all routes for UserController here
     * @param app
     */
    public index(app: Router): void {

        this.router.post("/sendOtp", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.sendOtp(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/verifyOtp", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.verifyOtp(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/uploadFileData", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.uploadFileData(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getConfigData/:configName?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.getConfigData(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/sendNotification/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.sendNotification(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        // this.router.post("/updateNotificationSeenStatus/:userType", async (req: Request, res: Response, next: NextFunction) => {

        //     try {

        //         const response = await this.common.updateNotificationSeenStatus(req, res);
        //         next({ isSuccess: true, data: response });
        //     } catch (error) {

        //         next({ isSuccess: false, data: error });
        //     }
        // });

        this.router.post("/getNotificationHistory/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.getNotificationHistory(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/sendEmailIdVerificationMail/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.sendEmailIdVerificationMail(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.get("/verifyEmailId/:userId/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.verifyEmailId(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/createOffer/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.createOffer(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/updatepOfferDetails/:_id/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.updatepOfferDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/updatepOfferMapping/:_id/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.updatepOfferMapping(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getOffersList/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.getOffersList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getOfferDetails/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.common.getOfferDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });
    }
}
