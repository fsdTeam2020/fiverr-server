import { NextFunction, Request, Response, Router } from "express";
import { C } from "../config/constants/constants";
import { CategoryController } from "../core/controllers/CategoryController";
import { ValidationMiddleware } from "../middlewares/ValidationMiddleware";

export class CategoryRoutes {

    public router: Router = Router();
    private category: CategoryController;

    constructor(app: Router) {

        this.index(app);
        this.category = new CategoryController();
    }

    /**
     * Define all routes for CategoryController here
     * @param app
     */
    public index(app: Router): void {

        this.router.post("/insertCategoryDetails/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.insertCategoryDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getCategoryList/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.getCategoryList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getAllCategoryList/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.getAllCategoryList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getCategoryDetails/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.getCategoryDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/updateCategoryDetails/:_id/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.updateCategoryDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/insertSubCategoryDetails/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.insertSubCategoryDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getSubCategoryList/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.getSubCategoryList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getAllSubCategoryList/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.getAllSubCategoryList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getSubCategoryDetails/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.getSubCategoryDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/updateSubCategoryDetails/:_id/:userType?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.category.updateSubCategoryDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });
    }
}
