import { Request, Response, Router } from "express";
import { C } from "../config/constants/constants";
import { ApiLogMiddleware } from "../middlewares/ApiLogMiddleware";

import { UserRoutes } from "./UserRoutes";
import { EncryptionRoutes } from "./EncryptionRoutes";
import { CommonRoutes } from "./CommonRoutes";
import { CategoryRoutes } from "./CategoryRoutes";
import { SellerProfileRoutes } from "./SellerProfileRoutes";
import { RoleRoutes } from "./RoleRoutes";

import { ResponseFormatter } from "../lib/ResponseFormatter";
import { MessagesConstants as msgConst } from "../config/constants/messages.constants";

export class Routes {

    public router: Router = Router();
    public userRoutes: UserRoutes;
    public encryptionRoutes: EncryptionRoutes;
    public commonRoutes: CommonRoutes;
    public categoryRoutes: CategoryRoutes;
    public sellerProfileRoutes: SellerProfileRoutes;
    public roleRoutes: RoleRoutes;

    constructor(app: Router) {

        this.userRoutes = new UserRoutes(app);
        this.encryptionRoutes = new EncryptionRoutes(app);
        this.commonRoutes = new CommonRoutes(app);
        this.categoryRoutes = new CategoryRoutes(app);
        this.sellerProfileRoutes = new SellerProfileRoutes(app);
        this.roleRoutes = new RoleRoutes(app);
        this.index(app);
    }

    /**
     * Define all routes here
     * @param app
     */
    public index(app: Router): void {

        this.router.get("/serverHealth", (req: Request, res: Response) => {
            res.status(200).send({ data: "Server Health - OK" });
        });

        app.use(ApiLogMiddleware.logApiRequest);

        this.initUserRoutes(app);
        this.initCipherRoutes(app);
        this.initCommonRoutes(app);
        this.initCategoryRoutes(app);
        this.initSellerProfileRoutes(app);
        this.initRoleRoutes(app);

        this.router.get("/", (req: Request, res: Response) => {

            res.status(200).send({ data: "OK" });
        });
    }

    private initUserRoutes(app: Router): void {

        app.use("/user", this.userRoutes.router);
    }

    private initCipherRoutes(app: Router): void {

        app.use("/cipher", this.encryptionRoutes.router);
    }

    private initCommonRoutes(app: Router): void {

        app.use("/common", this.commonRoutes.router);
    }

    private initCategoryRoutes(app: Router): void {

        app.use("/category", this.categoryRoutes.router);
    }

    private initSellerProfileRoutes(app: Router): void {

        app.use("/sellerProfile", this.sellerProfileRoutes.router);
    }

    private initRoleRoutes(app: Router): void {

        app.use("/role", this.roleRoutes.router);
    }
}
