import { NextFunction, Request, Response, Router } from "express";
import { C } from "../config/constants/constants";
import { UserController } from "../core/controllers/UserController";
import { ValidationMiddleware } from "../middlewares/ValidationMiddleware";

export class UserRoutes {

    public router: Router = Router();
    private user: UserController;

    constructor(app: Router) {

        this.index(app);
        this.user = new UserController();
    }

    /**
     * Define all routes for UserController here
     * @param app
     */
    public index(app: Router): void {

        this.router.post("/register", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.register(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/edit/:_id", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.edit(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/resetPassword", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.resetPassword(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/login", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.login(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/logout/:_id", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.logout(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getUserDetail/:_id/:userType", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.getUserDetail(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getUserList/:userType", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.getUserList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getAllUserList/:userType", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.getAllUserList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/markUnMarkFavoriteServices/:activityType", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.markingFavoriteServices(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getFavoriteServiceList", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.getFavoriteServiceList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getDashboardData", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.user.getDashboardData(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });
    }
}
