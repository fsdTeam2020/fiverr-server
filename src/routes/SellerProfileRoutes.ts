import { NextFunction, Request, Response, Router } from "express";
import { C } from "../config/constants/constants";
import { SellerProfileController } from "../core/controllers/SellerProfileController";
import { ValidationMiddleware } from "../middlewares/ValidationMiddleware";

export class SellerProfileRoutes {

    public router: Router = Router();
    private sellerProfile: SellerProfileController;

    constructor(app: Router) {

        this.index(app);
        this.sellerProfile = new SellerProfileController();
    }

    /**
     * Define all routes for CategoryController here
     * @param app
     */
    public index(app: Router): void {

        this.router.post("/createSellerService", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.createSellerService(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getSellerServiceList", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.getSellerServiceList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getAllSellerServiceList/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.getAllSellerServiceList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getSellerServiceDetails", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.getSellerServiceDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/searchServices", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.searchServices(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/createPostServiceRequest/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.createPostServiceRequest(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/updatepPostServiceRequestAcceptDetails/:_id/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.updatepPostServiceRequestAcceptDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getPostServiceRequestList/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.getPostServiceRequestList(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getPostServiceRequestDetails/:userType", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.sellerProfile.getPostServiceRequestDetails(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });
    }
}
