import { NextFunction, Request, Response, Router } from "express";
import { C } from "../config/constants/constants";
import { ValidationMiddleware } from "../middlewares/ValidationMiddleware";
import { EncryptionController } from "../core/controllers/EncryptionController";

export class EncryptionRoutes {

    public router: Router = Router();
    private cipher: EncryptionController;

    constructor(app: Router) {

        this.index(app);
        this.cipher = new EncryptionController();
    }

    /**
     * Define all routes for EncryptionController here
     * @param app
     */
    public index(app: Router): void {

        this.router.post("/encrypt", ValidationMiddleware.validate, async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.cipher.encrypt(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/decrypt", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.cipher.decrypt(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/search", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.cipher.search(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });
    }
}
