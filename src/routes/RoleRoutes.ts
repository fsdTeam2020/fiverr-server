import { NextFunction, Request, Response, Router } from "express";
import { C } from "../config/constants/constants";
import { RoleController } from "../core/controllers/RoleController";
import { ValidationMiddleware } from "../middlewares/ValidationMiddleware";

export class RoleRoutes {

    public router: Router = Router();
    private role: RoleController;

    constructor(app: Router) {

        this.index(app);
        this.role = new RoleController();
    }

    /**
     * Define all routes for CategoryController here
     * @param app
     */
    public index(app: Router): void {

        this.router.post("/add/:_id?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.addRole(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.put("/edit/:_id", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.editRole(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.put("/disable/:_id", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.disableRole(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.put("/enable/:_id", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.enableRole(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.put("/grantPermission", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.grantPermission(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.put("/revokePermission", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.revokePermission(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getRolePermissions/:_id", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.getRolePermissions(req, res, app);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });

        this.router.post("/getRoles/:_id?", async (req: Request, res: Response, next: NextFunction) => {

            try {

                const response = await this.role.getRoleDetail(req, res);
                next({ isSuccess: true, data: response });
            } catch (error) {

                next({ isSuccess: false, data: error });
            }
        });
    }
}
