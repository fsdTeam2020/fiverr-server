"use strict";

// https://stackoverflow.com/questions/35706164/typescript-import-as-vs-import-require
import cors from "cors";
import express from "express";
import fileUpload from "express-fileupload";
import helmet from "helmet";
// tslint:disable-next-line: no-var-requires
const robots = require("express-robots-txt");
import * as bodyParser from "body-parser";
import methodOverride from "method-override";
import * as path from "path";
import * as swaggerUi from "swagger-ui-express";

import * as config from "./config/config";
import { MongoDB } from "./config/database/mongoose";
import { MysqlDB } from "./config/database/sequelize";
import { ApiLogMiddleware } from "./middlewares/ApiLogMiddleware";
import { ResponseMiddleware } from "./middlewares/ResponseMiddleware";
/* import { RegisterRoutes } from "./routes/"; */
import { Routes } from "./routes/Routes";

export class App {

    public app: express.Express;
    public mongoDb: any;
    public mysqlDb: any;

    constructor() {

        this.app = express();
        this.mongoDb = new MongoDB();
        // this.mysqlDb = new MysqlDB();
        this.bootstrap();
    }

    public initServer(): void {

        this.app.listen(config.PORT, () => {

            console.log("Encryption Micro-Service initializing in " + config.NODE_ENV + " environment");
            console.log("Encryption Micro-Service server listening on port " + config.PORT);
        });
    }

    private bootstrap() {

        this.enableProxy();
        this.initFileUpload();
        this.initHelmet();
        this.initBodyParser();
        this.initMethodOverride();
        this.enableCORS();
        this.generateRobotsFile();
        this.initResources();

        this.initRoutes();
        this.app.use(ApiLogMiddleware.logApiRequest);
        this.defineSwaggerDoc();

        // it's position should be at last, final middleware to return response with success/error
        this.sendResponse();
    }

    private enableProxy(): void {

        // if your server is behind a proxy,
        this.app.enable("trust proxy");
    }

    private initFileUpload(): void {

        this.app.use(fileUpload());
    }

    private initHelmet(): void {

        this.app.use(helmet());
    }

    private initBodyParser(): void {

        // Url Encoded Data
        this.app.use(bodyParser.urlencoded({
            extended: true,
            limit: "50mb",
        }));

        // to Support JSON Encoded Bodies
        this.app.use(bodyParser.json({
            type: "application/json", limit: "50mb",
        }));
    }

    private initMethodOverride(): void {

        this.app.use(methodOverride("X-HTTP-Method-Override"));
    }

    private enableCORS(): void {

        this.app.use(cors({
            exposedHeaders: ["X-Total-Count", "Content-Type"],
            origin: "*",
        }));
    }

    private generateRobotsFile(): void {

        this.app.use(robots({ UserAgent: "*", Disallow: "/" }));
    }

    private initRoutes(): void {

        const routes = new Routes(this.app);
        this.app.use(routes.router);
    }

    private sendResponse(): void {

        this.app.use(ResponseMiddleware.sendResponse);
    }

    private defineSwaggerDoc(): void {

        /* RegisterRoutes(this.app); */
        // const swaggerDocument = require("../swagger.json");
        // this.app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    }

    private initResources(): void {

        // cache static resources for 24 hours
        const staticContentMaxAge = 1000 * 60 * 60 * 24;
        // static directory
        this.app.use(express.static(path.join(__dirname, "../resources"), { maxAge: staticContentMaxAge }));
    }
}
