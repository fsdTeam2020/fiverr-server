declare namespace Express {

    export interface Request {

        __apiLogRequestId?: string
        productType?: string
    }

    export interface Response {

        __responseData?: any
    }
}