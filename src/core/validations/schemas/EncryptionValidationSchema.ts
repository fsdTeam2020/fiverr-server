import Joi from "joi";

export class EncryptionValidationSchema {

    public static schemas: object = {

        "cipher/encryption": "getRegistrationRequestSchema",
        "cipher/decryption": "",
        "cipher/search": "getEditRequestSchema",
    };

    public static getRegistrationRequestSchema() {

    }

    constructor() {

    }
}
