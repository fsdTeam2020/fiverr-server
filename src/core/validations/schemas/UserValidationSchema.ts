import Joi from "joi";
import { get, has } from "lodash";

export class UserValidationSchema {

    public static schemas: object = {

        "user/register": "getRegistrationRequestSchema",
        "user/authenticate": "",
        "user/edit": "getEditRequestSchema",
    };

    // public static getSchema(action: string) {

    //     if(has(UserValidationSchema.schemas, action)) {
    //         return get(UserValidationSchema.schemas, action);
    //     }

    //     return '';
    // }

    public static getRegistrationRequestSchema() {

        const schema = Joi.object().keys({
            username: UserValidationSchema.username.required(),
            password: Joi.string().min(6).max(20).required(),
            firstName: Joi.string().min(3).max(100).required(),
            middleName: Joi.string().min(3).max(100).optional(),
            lastName: Joi.string().min(3).max(100).optional(),
        });

        return schema;
    }

    public static getEditRequestSchema() {

        const schema = Joi.object().keys({
            _id: Joi.string().alphanum().required(),
            username: this.username.optional(),
            password: Joi.string().min(6).max(20).required(),
            firstName: Joi.string().min(3).max(100).required(),
            middleName: Joi.string().min(3).max(100).optional(),
            lastName: Joi.string().min(3).max(100).optional(),
        });

        return schema;
    }

    private static username = Joi.string().alphanum().min(3).max(100);
    private static password = Joi.string().alphanum().min(3).max(100).required();
    private static firstName = Joi.string().alphanum().min(3).max(100).required();
    private static middleName = Joi.string().alphanum().min(3).max(100).required();
    private static lastName = Joi.string().alphanum().min(3).max(100).required();

    constructor() {

    }
}
