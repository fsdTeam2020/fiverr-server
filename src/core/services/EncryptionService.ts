import crypto from "crypto";

export class EncryptionService {

    public static encrypt(text: string, algorithm: string, password: string) {

        const cipher = crypto.createCipher(algorithm, password);
        let crypted = cipher.update(text, "utf8", "hex");
        crypted += cipher.final("hex");
        return crypted;
    }

    public static decrypt(text: string, algorithm: string, password: string) {

        const decipher = crypto.createDecipher(algorithm, password);
        let dec = decipher.update(text, "hex", "utf8");
        dec += decipher.final("utf8");
        return dec;
    }

    constructor() {

    }
}
