
import { Admin } from "../../config/notification/firebase";
import { Utils } from "../../lib/Utils";
import { NotificationModel } from "../models/NotificationModel";
import { Types } from "mongoose";

export class NotificationService {

    public static async sendNotification(userData: any, data: any) {

        try {
            // These registration tokens come from the client FCM SDKs.
            // Note: You can send messages to up to 1000 devices in a single request.
            // If you provide an array with over 1000 registration tokens, the request
            // will fail with a messaging/invalid-recipient error.
            // const registrationTokens = [
            //     "bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1",
            //     "ecupwIfBy1w:APA91bFtuMY7MktgxA3Au_Qx7cKqnf",
            // ];

            const registrationTokens: any = [];
            for (const userObj of userData) {
                if (!Utils.isEmpty(userObj.notificationToken)) {
                    registrationTokens.push(userObj.notificationToken);
                }
            }

            // See the "Defining the message payload" section below for details
            // on how to define a message payload.
            // const payload = {
            //     notification: {
            //         score: "850",
            //         time: "2:45",
            //     },
            //     data: {
            //         stock: "GOOG",
            //         open: "829.62",
            //         close: "635.67",
            //       },
            // };

            const payload: any = {
                notification: data,
            };

            // Set the message as high priority and have it expire after 24 hours.
            const options: any = {
                priority: "high",
                timeToLive: 60 * 60 * 24,
            };

            const notificationData: any = {
                body: payload,
            };
            // Send a message to the device corresponding to the provided
            // registration token with the provided options.
            const response: any = await Admin.messaging().sendToDevice(registrationTokens, payload, options);
            const failedTokens: any = [];
            response.results.forEach((resp: any, idx: number) => {
                const userId: any = userData[idx].id;
                if (Utils.isEmpty(resp.messageId)) {
                    failedTokens.push(userId);
                    notificationData.sendStatus = false;
                } else {
                    notificationData.sendStatus = true;
                }
                notificationData.userMappingKey = Types.ObjectId(userId);
                notificationData.notificationServerResponse = resp;
                const notificationModel = new NotificationModel(notificationData);
                const logData = notificationModel.save();
            });
            if (response.failureCount > 0) {
                console.log("List of tokens that caused failures: ", failedTokens);
                return { response: failedTokens, status: false };
            } else {
                console.log("Notification send successfully.");
                return { response, status: true };
            }

        } catch (exception) {
            console.log("Error sending message:", exception);
            return { response: exception.message, status: false };
        }
    }

    constructor() {

    }
}
