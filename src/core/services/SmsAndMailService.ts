import Twilio from "twilio";
import sgMail from "@sendgrid/mail";

export class SmsAndMailService {

    public static async sendSms(body: string, to: string, configData: any) {

        try {
            const accountSid = configData.accountSid; // Your Account SID from www.twilio.com/console
            const authToken = configData.authToken;   // Your Auth Token from www.twilio.com/console
            const from = configData.sendingFrom; // From a valid Twilio number

            const client = Twilio(accountSid, authToken);
            const message: any = await client.messages.create({ body, to, from });
            return { response: message, status: true };
        } catch (exception) {
            console.log(exception);
            return { response: exception.message, status: false };
        }
    }

    public static async sendMail(subject: string, body: string, to: string, configData: any) {

        try {
            sgMail.setApiKey(configData.apiKey);
            const msg = {
                to,
                from: configData.sendingFrom, // Use the email address or domain you verified above
                subject,
                // text: "and easy to do anywhere, even with Node.js",
                html: body,
            };

            const message: any = await sgMail.send(msg);
            return { response: "Mail Send Successfully", status: true };
        } catch (exception) {
            console.log(exception);
            if (exception.response) {
                return { response: exception.response.body, status: false };
            } else {
                return { response: exception.message, status: false };
            }

        }
    }

    constructor() {

    }
}
