import * as jwt from "jsonwebtoken";
import { C } from "../../config/constants/constants";
import { UserModel } from "../models/UserModel";
import { ResponseFormatter } from "../../lib/ResponseFormatter";
import { MessagesConstants as MSG } from "../../config/constants/messages.constants";
import { Utils } from "../../lib/Utils";

export class JwtTokenService {

    public static generateToken(userObj: any) {

        const payload: any = { _id: userObj._id };
        const options: any = {
            expiresIn: userObj.tokenExpirationIn, // in seconds (0 for token Expiration, -1 for infinite Expiration)
        };
        const token: string = jwt.sign(payload, C.AUTH_KEY, options);
        return token;
    }

    public static async verifyToken(token: any) {

        try {
            const params = { token, status: true };
            const userObj: any = await UserModel.findOne(params);

            if (Utils.isEmpty(userObj)) {
                return false;
            } else {
                const options: any = {};
                if (userObj.tokenExpirationIn === -1) {
                    options.ignoreExpiration = true;
                }
                const verificationStatus = jwt.verify(token, C.AUTH_KEY, options);
                if (Utils.isEmpty(verificationStatus)) {
                    return false;
                } else {
                    return true;
                }
            }

        } catch (exception) {
            return false;
        }
    }

    constructor() {

    }
}
