import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import { ISellerServices } from "../interfaces/ISellerServices";

export interface ISellerServicesModel extends ISellerServices, Document {

}

const SellerServicesModelSchema: Schema = new Schema(
    {
        title: { type: String },
        category: { type: Schema.Types.ObjectId, index: true, ref: "CategoryModel" },
        subCategory: { type: Schema.Types.ObjectId, index: true, ref: "SubCategoryModel" },
        serviceType: { type: String },
        platform: { type: String },
        expertise: { type: String },
        packages: [{
            packageType: { type: String },
            packageName: { type: String },
            packageDescription: { type: String },
            deliveryTime: { type: String },
            mobileOS: { type: String },
            revisions: { type: Number },
            price: { type: Number, default: 0 },
            // minPrice: { type: Number },
            // maxPrice: { type: Number },
            currencyType: { type: String },
            services: [{ type: String }],
            extraServices: [{
                serviceName: { type: String },
                revisions: { type: Number },
                price: { type: Number, default: 0 },
                // minPrice: { type: Number },
                // maxPrice: { type: Number },
                currencyType: { type: String },
            }],
        }],
        description: { type: String },
        faq: [{
            question: { type: String },
            answer: { type: String },
        }],
        rating: {
            1: { type: Number, default: 0 },
            2: { type: Number, default: 0 },
            3: { type: Number, default: 0 },
            4: { type: Number, default: 0 },
            5: { type: Number, default: 0 },
        },
        requirementDetails: { type: String },
        imageList: [{ type: String }],
        videoList: [{ type: String }],
        pdfList: [{ type: String }],
        offers: [{ type: Schema.Types.ObjectId, index: true, ref: "OffersModel" }],
        status: { type: Boolean, index: true, default: true },
        userMappingKey: { type: Schema.Types.ObjectId, index: true, ref: "UserModel" },
        createdAt: { type: Date },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "seller_services",
        timestamps: true,
    },
);

SellerServicesModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

SellerServicesModelSchema.plugin(mongoosePaginate);

export const SellerServicesModel = model<ISellerServicesModel>("SellerServicesModel", SellerServicesModelSchema);
