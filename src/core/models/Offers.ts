import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import { IOffers } from "../interfaces/IOffers";

export interface IOffersModel extends IOffers, Document {

}

const OffersModelSchema: Schema = new Schema(
    {
        title: { type: String },
        discountPercentage: { type: Number, default: 0 },
        validFrom: { type: Date, index: true },
        validTo: { type: Date, index: true },
        status: { type: Boolean, index: true, default: true },
        createdAt: { type: Date, index: true },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "offers",
        timestamps: true,
    },
);

OffersModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

OffersModelSchema.plugin(mongoosePaginate);

export const OffersModel = model<IOffersModel>("OffersModel", OffersModelSchema);
