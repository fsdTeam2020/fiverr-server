import { Document, model, Schema } from "mongoose";
import { IUserActivity } from "../interfaces/IUserActivity";

export interface IUserActivityModel extends IUserActivity, Document {

}

const UserActivityModelSchema: Schema = new Schema(
    {
        recentSearchServices: [{ type: Schema.Types.ObjectId, index: true, ref: "SellerServicesModel" }],
        savedServices: [{ type: Schema.Types.ObjectId, index: true, ref: "SellerServicesModel" }],
        purchasedServices: [{ type: Schema.Types.ObjectId, index: true, ref: "SellerServicesModel" }],
        favoriteServices: [{ type: Schema.Types.ObjectId, index: true, ref: "SellerServicesModel" }],
        userMappingKey: { type: Schema.Types.ObjectId, index: true, unique: true, ref: "UserModel" },
        createdAt: { type: Date },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "user_activity",
        timestamps: true,
    },
);

UserActivityModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

export const UserActivityModel = model<IUserActivityModel>("UserActivityModel", UserActivityModelSchema);
