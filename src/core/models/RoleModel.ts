import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import { IRole } from "../interfaces/IRole";

export interface IRoleModel extends IRole, Document {

}

const RoleModelSchema: Schema = new Schema(
    {
        role: { type: String },
        roleKey: { type: String, index: { unique: true }, required: true },
        sourceId: { type: String },
        status: { type: Boolean, index: true, default: true },
        createdAt: { type: Date },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "roles",
        timestamps: true,
    },
);

RoleModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

RoleModelSchema.plugin(mongoosePaginate);

export const RoleModel = model<IRoleModel>("RoleModel", RoleModelSchema);
