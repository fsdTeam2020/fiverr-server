import { Document, model, Schema } from "mongoose";
import { IConfig } from "../interfaces/IConfig";

export interface IConfigModel extends IConfig, Document {

}

const ConfigModelSchema: Schema = new Schema(
    {
        configName: { type: String, index: { unique: true } },
        configValue: { type: Schema.Types.Mixed },
        status: { type: Boolean, index: true, default: true },
        createdAt: { type: Date },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "configuration",
        timestamps: true,
    },
);

ConfigModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

export const ConfigModel = model<IConfigModel>("ConfigModel", ConfigModelSchema);
