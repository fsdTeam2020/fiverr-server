import { Document, model, Schema } from "mongoose";
import { ISmsAndMail } from "../interfaces/ISmsAndMail";
import { C } from "../../config/constants/constants";
import _ from "underscore";

export interface ISmsAndMailModel extends ISmsAndMail, Document {

}

const SmsAndMailModelSchema: Schema = new Schema(
    {
        source: { type: String, enum: _(C.OTP_SOURCES).toArray() },
        from: { type: String },
        to: { type: String, index: true },
        otp: { type: String, index: true },
        body: { type: String },
        sendStatus: { type: Boolean, index: true, default: false },
        sourceServerResponse: { type: String },
        createdAt: { type: Date, index: true },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "sms_and_mail",
        timestamps: true,
    },
);

SmsAndMailModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

export const SmsAndMailModel = model<ISmsAndMailModel>("SmsAndMailModel", SmsAndMailModelSchema);
