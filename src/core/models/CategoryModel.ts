import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import { ICategory } from "../interfaces/ICategory";

export interface ICategoryModel extends ICategory, Document {

}

const CategoryModelSchema: Schema = new Schema(
    {
        name: { type: String, required: true },
        description: { type: String },
        image: { type: String },
        isPopularCategory: { type: Boolean, default: false },
        status: { type: Boolean, index: true, default: true },
        subCategoryMappingKey: [{ type: Schema.Types.ObjectId, index: true, ref: "SubCategoryModel" }],
        createdAt: { type: Date },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "categories",
        timestamps: true,
    },
);

CategoryModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

CategoryModelSchema.plugin(mongoosePaginate);

export const CategoryModel = model<ICategoryModel>("CategoryModel", CategoryModelSchema);
