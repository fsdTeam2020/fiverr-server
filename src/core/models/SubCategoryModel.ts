import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import { ISubCategory } from "../interfaces/ISubCategory";

export interface ISubCategoryModel extends ISubCategory, Document {

}

const SubCategoryModelSchema: Schema = new Schema(
    {
        name: { type: String, required: true },
        description: { type: String },
        groupName: { type: String },
        // image: { type: String },
        serviceTypes: [{ type: String }],
        platforms: [{ type: String }],
        deliveryTime: [{ type: String }],
        revisions: [{ type: Number }],
        packages: [{
            packageType: { type: String },
            services: [{ type: String }],
            minPrice: { type: Number, default: 5 },
            maxPrice: { type: Number, default: 995 },
            currencyType: { type: String, default: "$" },
        }],
        status: { type: Boolean, index: true, default: true },
        createdAt: { type: Date },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "subCategories",
        timestamps: true,
    },
);

SubCategoryModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

SubCategoryModelSchema.plugin(mongoosePaginate);

export const SubCategoryModel = model<ISubCategoryModel>("SubCategoryModel", SubCategoryModelSchema);
