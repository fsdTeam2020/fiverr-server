import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import { IPostServiceRequest } from "../interfaces/IPostServiceRequest";

export interface IPostServiceRequestModel extends IPostServiceRequest, Document {

}

const PostServiceRequestModelSchema: Schema = new Schema(
    {
        description: { type: String },
        category: { type: Schema.Types.ObjectId, index: true, ref: "CategoryModel" },
        subCategory: { type: Schema.Types.ObjectId, index: true, ref: "SubCategoryModel" },
        deliveryTime: { type: String },
        price: { type: Number, default: 0 },
        currencyType: { type: String },
        imageList: [{ type: String }],
        offerAcceptList: [{
            userMappingKey: { type: Schema.Types.ObjectId, index: true, ref: "UserModel" },
            description: { type: String },
            deliveryTime: { type: String },
            createdAt: { type: Date, default: Date.now },
        }],
        status: { type: Boolean, index: true, default: true },
        createdAt: { type: Date, index: true },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "post_service_request",
        timestamps: true,
    },
);

PostServiceRequestModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

PostServiceRequestModelSchema.plugin(mongoosePaginate);

export const PostServiceRequestModel = model<IPostServiceRequestModel>("PostServiceRequestModel", PostServiceRequestModelSchema);
