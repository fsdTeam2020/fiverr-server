import { Document, model, Schema } from "mongoose";
import { IEncryptedData } from "../interfaces/IEncryptedData";

const EncryptedDataModelSchema: Schema = new Schema(
    {
        value: Schema.Types.Mixed,
    },
    {
        collection: "encrypted_data",
        timestamps: true,
    },
);

export const EncryptedDataModel = model<IEncryptedData>("EncryptedDataModel", EncryptedDataModelSchema);
