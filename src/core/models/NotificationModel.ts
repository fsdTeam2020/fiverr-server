import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import { INotification } from "../interfaces/INotification";

export interface INotificationModel extends INotification, Document {

}

const NotificationModelSchema: Schema = new Schema(
    {
        body: { type: Object },
        userMappingKey: { type: Schema.Types.ObjectId, index: true, ref: "UserModel" },
        isSeen: { type: Boolean, default: false },
        sendStatus: { type: Boolean, index: true, default: false },
        notificationServerResponse: { type: Object },
        createdAt: { type: Date, index: true },
        updatedAt: { type: Date, default: Date.now },
    },
    {
        collection: "notification",
        timestamps: true,
    },
);

NotificationModelSchema.pre("save", function(next) {
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }
    next();
});

NotificationModelSchema.plugin(mongoosePaginate);

export const NotificationModel = model<INotificationModel>("NotificationModel", NotificationModelSchema);
