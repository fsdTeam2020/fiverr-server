import { Document, model, Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate";
import bcrypt from "bcryptjs";
import { C } from "../../config/constants/constants";
import _ from "underscore";
import { IUser } from "../interfaces/IUser";
const SALT_WORK_FACTOR = 10;

export interface IUserModel extends IUser, Document {

    comparePassword(password: string): string;
    encryptPassword(password: string): string;
}

const UserModelSchema: Schema = new Schema(
    {
        userName: { type: String, index: { unique: true }, required: true },
        password: { type: String, required: true },
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        gender: { type: String, enum: _(C.GENDER).toArray(), required: true },
        mobileNumber: { type: String, required: true },
        isMobileNumberVerified: { type: Boolean, default: false },
        emailId: { type: String, required: true },
        isEmailIdVerified: { type: Boolean, default: false },
        dob: { type: Date, required: true },
        image: { type: String, default: "" },
        role: { type: String, default: "" },
        rating: {
            1: { type: Number, default: 0 },
            2: { type: Number, default: 0 },
            3: { type: Number, default: 0 },
            4: { type: Number, default: 0 },
            5: { type: Number, default: 0 },
        },
        countryName: { type: String, default: "" },
        memberSince: { type: Date, default: Date.now },
        recentDelivery: { type: String, default: "" },
        description: { type: String, default: "" },
        languagesKnown: [{ type: String, default: "" }],
        // skills: [{ type: String, default: "" }],
        occupation: {
            name: { type: String, default: "" },
            skills: [{ type: String, default: "" }],
            fromYear: { type: String, default: "" },
            toYear: { type: String, default: "" },
        },
        educations: [{
            degree: { type: String, default: "" },
            institution: { type: String, default: "" },
            yearOfPassing: { type: Number, default: 0 },
        }],
        certifications: [{
            year: { type: String, default: "" },
            certName: { type: String, default: "" },
            certFrom: { type: String, default: "" },
        }],
        isFacebookConnect: { type: Boolean, default: false },
        isLinkedInConnect: { type: Boolean, default: false },
        userCategoryType: { type: String, enum: _(C.USER_CATEGORY_TYPE).toArray(), default: C.USER_CATEGORY_TYPE.BEGINNER, index: true, required: true },
        notificationToken: { type: String },
        isOnline: { type: Boolean, index: true, default: true },
        userType: { type: String, enum: _(C.USER_TYPE).toArray(), default: C.USER_TYPE.CUSTOMER, index: true, required: true },
        havingSellerPermission: { type: Boolean, default: false },
        token: { type: String, index: true },
        tokenExpirationIn: { type: Number, default: 60 * 60 * 24 * 30 },  // 30 days, in seconds
        loginAt: { type: Date },
        logoutAt: { type: Date },
        createdAt: { type: Date },
        updatedAt: { type: Date, default: Date.now },
        status: { type: Boolean, index: true, default: true },
    },
    {
        collection: "users",
        timestamps: true,
    },
);

// Saves the user's password hashed (plain text password storage is not good)
UserModelSchema.pre("save", function(next) {

    const user: any = this;
    const createAt = new Date();
    if (!this.get("createdAt")) {
        this.set("createdAt", createAt);
    }

    if (user.isModified("password") || this.isNew) {
        user.encryptPassword(user.password).then(function(hash: string) {
            user.password = hash;
            next();
        }, function(err: any) {
            return next(err);
        });

    } else {
        return next();
    }
});

// Create method to compare password input to password saved in database
UserModelSchema.methods.comparePassword = async function(pw: string) {
    try {

        const isMatch = await bcrypt.compare(pw, this.password);
        return Promise.resolve(isMatch);
    } catch (exception) {

        return Promise.reject(exception);
    }
};

// Create method to encrypt password input
UserModelSchema.methods.encryptPassword = async function(password: string) {
    try {

        const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
        const hash = await bcrypt.hash(password, salt);
        return Promise.resolve(hash);
    } catch (exception) {

        return Promise.reject(exception);
    }
};

UserModelSchema.plugin(mongoosePaginate);

export const UserModel = model<IUserModel>("UserModel", UserModelSchema);
