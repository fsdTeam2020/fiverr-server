import { Document } from "mongoose";

export interface IRole extends Document {

    role: string;
    roleKey: string;
    sourceId: string;
    status: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}
