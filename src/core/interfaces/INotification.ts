import { Document, Schema } from "mongoose";

export interface INotification extends Document {

    body: object;
    userMappingKey: Schema.Types.ObjectId;
    isSeen: boolean;
    sendStatus: boolean;
    notificationServerResponse: object;
    createdAt?: Date;
    updatedAt?: Date;
}
