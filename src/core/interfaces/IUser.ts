import { Document } from "mongoose";

export interface IUser extends Document {

    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    gender: string;
    mobileNumber: string;
    isMobileNumberVerified: boolean;
    emailId: string;
    isEmailIdVerified: boolean;
    dob: Date;
    image: string;
    role: string;
    rating: {
        1: number;
        2: number;
        3: number;
        4: number;
        5: number;
    };
    countryName: string;
    memberSince: Date;
    recentDelivery: string;
    description: string;
    languagesKnown: [string];
    // skills: [string];
    occupation: {
        name: string;
        skills: [string];
        fromYear: string;
        toYear: string;
    };
    educations: [{
        degree: string;
        institution: string;
        yearOfPassing: number;
    }];
    certifications: [{
        year: string;
        certName: string;
        certFrom: string;
    }];
    isFacebookConnect: boolean;
    isLinkedInConnect: boolean;
    userCategoryType: string;
    notificationToken: string;
    isOnline: boolean;
    userType: string;
    havingSellerPermission: boolean;
    token: string;
    tokenExpirationIn: number;
    loginAt: Date;
    logoutAt: Date;
    createdAt?: Date;
    updatedAt?: Date;
    status: boolean;
}
