import { Document } from "mongoose";

export interface ISmsAndMail extends Document {

    source: string;
    from: string;
    to: string;
    otp: number;
    body: string;
    sendStatus: boolean;
    sourceServerResponse: string;
    createdAt?: Date;
    updatedAt?: Date;
}
