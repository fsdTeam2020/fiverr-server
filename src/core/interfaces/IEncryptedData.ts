import { Document } from "mongoose";

export interface IEncryptedData extends Document {

    value: string;
    createdAt?: Date;
    updatedAt?: Date;
}
