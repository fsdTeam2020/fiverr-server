import { Document, Schema } from "mongoose";

export interface IPostServiceRequest extends Document {

    description: string;
    category: Schema.Types.ObjectId;
    subCategory: Schema.Types.ObjectId;
    deliveryTime: string;
    price: number;
    currencyType: string;
    imageList: [string];
    offerAcceptList: [{
        userMappingKey: Schema.Types.ObjectId;
        description: string;
        deliveryTime: string;
    }];
    status: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}
