import { Document } from "mongoose";

export interface ISubCategory extends Document {

    name: string;
    description: string;
    groupName: string;
    // image: string;
    serviceTypes: [string];
    platforms: [string];
    deliveryTime: [string];
    revisions: [number];
    packages: [{
        packageType: string;
        services: [string];
        minPrice: number;
        maxPrice: number;
        currencyType: string;
    }];
    status: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}
