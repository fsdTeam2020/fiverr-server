import { Document, Schema } from "mongoose";

export interface ICategory extends Document {

    name: string;
    description: string;
    image: string;
    isPopularCategory: boolean;
    status: boolean;
    subCategoryMappingKey: [Schema.Types.ObjectId];
    createdAt?: Date;
    updatedAt?: Date;
}
