import { Document, Schema } from "mongoose";

export interface IConfig extends Document {

    configName: string;
    configValue: Schema.Types.Mixed;
    status: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}
