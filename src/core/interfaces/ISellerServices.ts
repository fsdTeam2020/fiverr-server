import { Document, Schema } from "mongoose";

export interface ISellerServices extends Document {

    title: string;
    category: Schema.Types.ObjectId;
    subCategory: Schema.Types.ObjectId;
    serviceType: string;
    platform: string;
    expertise: string;
    packages: [{
        packageType: string;
        packageName: string;
        packageDescription: string;
        deliveryTime: string;
        mobileOS: string;
        revisions: number;
        price: number;
        // minPrice: number;
        // maxPrice: number;
        currencyType: string;
        services: [string];
        extraServices: [{
            serviceName: string;
            revisions: number;
            price: number;
            // minPrice: number;
            // maxPrice: number;
            currencyType: string;
        }];
    }];
    description: string;
    faq: [{
        question: string;
        answer: string;
    }];
    rating: {
        1: number;
        2: number;
        3: number;
        4: number;
        5: number;
    };
    requirementDetails: string;
    imageList: [string];
    videoList: [string];
    pdfList: [string];
    offers: [Schema.Types.ObjectId];
    status: boolean;
    userMappingKey: Schema.Types.ObjectId;
    createdAt?: Date;
    updatedAt?: Date;
}
