import { Document } from "mongoose";

export interface IOffers extends Document {

    title: string;
    discountPercentage: number;
    validFrom: Date;
    validTo: Date;
    status: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}
