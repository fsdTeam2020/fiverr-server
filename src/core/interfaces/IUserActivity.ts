import { Document, Schema } from "mongoose";

export interface IUserActivity extends Document {

    recentSearchServices: [Schema.Types.ObjectId];
    savedServices: [Schema.Types.ObjectId];
    purchasedServices: [Schema.Types.ObjectId];
    favoriteServices: [Schema.Types.ObjectId];
    userMappingKey: Schema.Types.ObjectId;
    createdAt?: Date;
    updatedAt?: Date;
}
