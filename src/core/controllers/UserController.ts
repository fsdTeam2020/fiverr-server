import { Request, Response, Router } from "express";

import { Utils } from "../../lib/Utils";
import { C } from "../../config/constants/constants";
import _ from "underscore";
import { JwtTokenService } from "../services/JwtTokenService";
import moment = require("moment-timezone");
moment.tz.setDefault(C.TIMEZONE);
import { ResponseFormatter } from "../../lib/ResponseFormatter";
import { MessagesConstants as MSG } from "../../config/constants/messages.constants";
import { Types } from "mongoose";

import { UserModel } from "../models/UserModel";
import { UserActivityModel } from "../models/UserActivityModel";
import { SellerServicesModel } from "../models/SellerServicesModel";

export class UserController {

    constructor() {

    }

    public async register(req: Request, res: Response) {

        try {

            const userData = await UserModel.findOne({ userName: req.body.userName });
            if (userData) {

                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC001", MSG.USER_ALREADY_REGISTER, MSG.USER_ALREADY_REGISTER, MSG.USER_ALREADY_REGISTER);
                return Promise.reject(error);
            }

            const userDataObj: any = {
                userName: req.body.userName,
                password: req.body.password,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                mobileNumber: req.body.mobileNumber,
                emailId: req.body.emailId,
            };

            if (!Utils.isEmpty(req.body.gender)) {
                userDataObj.gender = req.body.gender;
            }

            if (!Utils.isEmpty(req.body.role)) {
                userDataObj.role = req.body.role;
            }

            if (!Utils.isEmpty(req.body.dob)) {
                userDataObj.dob = Utils.getDateFormatForDB(req.body.dob);
            }

            if (!Utils.isEmpty(req.body.userType)) {
                userDataObj.userType = req.body.userType;
            }

            const user = new UserModel(userDataObj);

            const data = await user.save();
            const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
            return Promise.resolve(success);
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC002", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async resetPassword(req: Request, res: Response) {

        try {
            const user = await UserModel.findOne({ userName: req.body.userName, status: true });
            if (!user) {

                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC003", MSG.INVALID_USER_NAME_PASSWORD, MSG.INVALID_USER_NAME_PASSWORD, MSG.INVALID_USER_NAME_PASSWORD);
                return Promise.reject(error);
            }
            const passwordHash = await user.encryptPassword(req.body.password);
            const updateData = {
                password: passwordHash,
            };

            const data = await UserModel.findByIdAndUpdate(user._id, updateData, { new: true });
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC004", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC005", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async edit(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC006", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const userTypeList: any = _(C.USER_TYPE).toArray();
            if (userTypeList.indexOf(req.body.userType) === -1) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC007", MSG.INVALID_REQUEST, MSG.USER_TYPE_REQUIRED, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            const updateData: any = {};

            if (!Utils.isEmpty(req.body.firstName)) {
                updateData.firstName = req.body.firstName;
            }

            if (!Utils.isEmpty(req.body.lastName)) {
                updateData.lastName = req.body.lastName;
            }

            if (!Utils.isEmpty(req.body.gender)) {
                updateData.gender = req.body.gender;
            }

            if (!Utils.isEmpty(req.body.mobileNumber) || !Utils.isEmpty(req.body.emailId)) {

                const fields: any = { mobileNumber: 1, emailId: 1, isMobileNumberVerified: 1, isEmailIdVerified: 1 };
                const userData: any = await UserModel.findById(Types.ObjectId(req.params._id), fields);

                if (!Utils.isEmpty(req.body.mobileNumber)) {
                    updateData.mobileNumber = req.body.mobileNumber;
                    updateData.isMobileNumberVerified = (req.body.mobileNumber === userData.mobileNumber) ? userData.isMobileNumberVerified : false;
                }

                if (!Utils.isEmpty(req.body.emailId)) {
                    updateData.emailId = req.body.emailId;
                    updateData.isEmailIdVerified = (req.body.emailId === userData.emailId) ? userData.isEmailIdVerified : false;
                }
            }

            if (!Utils.isEmpty(req.body.dob)) {
                updateData.dob = Utils.getDateFormatForDB(req.body.dob);
            }

            if (!Utils.isEmpty(req.body.notificationToken)) {
                updateData.notificationToken = req.body.notificationToken;
            }

            if (Utils.isSet(req.body.isMobileNumberVerified)) {
                updateData.isMobileNumberVerified = (/true/i).test(req.body.isMobileNumberVerified);
            }

            if (Utils.isSet(req.body.isEmailIdVerified)) {
                updateData.isEmailIdVerified = (/true/i).test(req.body.isEmailIdVerified);
            }

            if (!Utils.isEmpty(req.body.userType) && req.body.userType === C.USER_TYPE.SELLER) {

                updateData.userType = C.USER_TYPE.SELLER;
                updateData.havingSellerPermission = true;

                if (!Utils.isEmpty(req.body.role)) {
                    updateData.role = req.body.role;
                }

                if (!Utils.isEmpty(req.body.countryName)) {
                    updateData.countryName = req.body.countryName;
                }

                if (!Utils.isEmpty(req.body.memberSince)) {
                    updateData.memberSince = Utils.getDateFormatForDB(req.body.memberSince);
                }

                if (!Utils.isEmpty(req.body.description)) {
                    updateData.description = req.body.description;
                }

                if (!Utils.isEmpty(req.body.languagesKnown)) {
                    let languagesKnownList = [];
                    if (Array.isArray(req.body.languagesKnown)) {
                        languagesKnownList = req.body.languagesKnown;
                    } else {
                        languagesKnownList.push(req.body.languagesKnown);
                    }
                    updateData.languagesKnown = languagesKnownList;
                }

                // if (!Utils.isEmpty(req.body.skills)) {
                //     let skillsList = [];
                //     if (Array.isArray(req.body.skills)) {
                //         skillsList = req.body.skills;
                //     } else {
                //         skillsList.push(req.body.skills);
                //     }
                //     updateData.skills = skillsList;
                // }

                if (!Utils.isEmpty(req.body.occupation)) {
                    const occupationObj: any = {};
                    if (!Utils.isEmpty(req.body.occupation.name)) {
                        occupationObj.name = req.body.occupation.name;
                    }

                    if (!Utils.isEmpty(req.body.occupation.skills)) {
                        let skillsList = [];
                        if (Array.isArray(req.body.occupation.skills)) {
                            skillsList = req.body.occupation.skills;
                        } else {
                            skillsList.push(req.body.occupation.skills);
                        }
                        occupationObj.skills = skillsList;
                    }

                    if (!Utils.isEmpty(req.body.occupation.fromYear)) {
                        occupationObj.fromYear = req.body.occupation.fromYear;
                    }

                    if (!Utils.isEmpty(req.body.occupation.toYear)) {
                        occupationObj.toYear = req.body.occupation.toYear;
                    }

                    if (!Utils.isEmpty(occupationObj)) {
                        updateData.occupation = occupationObj;
                    }

                }

                if (!Utils.isEmpty(req.body.educations)) {
                    const educationsObj: any = [];
                    for (const educationData of req.body.educations) {
                        const educationObj = {
                            degree: !Utils.isEmpty(educationData.degree) ? educationData.degree : "",
                            institution: !Utils.isEmpty(educationData.institution) ? educationData.institution : "",
                            yearOfPassing: !Utils.isEmpty(educationData.yearOfPassing) ? educationData.yearOfPassing : 0,
                        };
                        educationsObj.push(educationObj);
                    }
                    updateData.educations = educationsObj;
                }

                if (!Utils.isEmpty(req.body.certifications)) {
                    const certificationsObj: any = [];
                    for (const certificationData of req.body.certifications) {
                        const certificationObj = {
                            year: !Utils.isEmpty(certificationData.year) ? certificationData.year : "",
                            certName: !Utils.isEmpty(certificationData.certName) ? certificationData.certName : "",
                            certFrom: !Utils.isEmpty(certificationData.certFrom) ? certificationData.certFrom : "",
                        };
                        certificationsObj.push(certificationObj);
                    }
                    updateData.certifications = certificationsObj;
                }

                if (!Utils.isSet(req.body.isFacebookConnect)) {
                    updateData.isFacebookConnect = (/true/i).test(req.body.isFacebookConnect);
                }

                if (!Utils.isSet(req.body.isLinkedInConnect)) {
                    updateData.isLinkedInConnect = (/true/i).test(req.body.isLinkedInConnect);
                }

                if (Utils.isSet(req.body.isOnline)) {
                    updateData.isOnline = (/true/i).test(req.body.isOnline);
                }
            } else if (!Utils.isEmpty(req.body.userType) && req.body.userType === C.USER_TYPE.ADMIN) {
                updateData.userType = C.USER_TYPE.ADMIN;

                if (!Utils.isEmpty(req.body.role)) {
                    updateData.role = req.body.role;
                }
            } else {
                updateData.userType = C.USER_TYPE.CUSTOMER;
            }

            const data = await UserModel.findByIdAndUpdate(req.params._id, updateData, { new: true });
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC008", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC009", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getUserDetail(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "UC010", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            const fields: any = { userType: 1, firstName: 1, lastName: 1, gender: 1, mobileNumber: 1, emailId: 1, dob: 1, isEmailIdVerified: 1, isMobileNumberVerified: 1, havingSellerPermission: 1 };
            if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.SELLER) {
                fields.image = 1;
                fields.role = 1;
                fields.rating = 1;
                fields.countryName = 1;
                fields.memberSince = 1;
                fields.recentDelivery = 1;
                fields.description = 1;
                fields.languagesKnown = 1;
                fields.occupation = 1;
                fields.educations = 1;
                fields.certifications = 1;
                fields.isFacebookConnect = 1;
                fields.isLinkedInConnect = 1;
                fields.userCategoryType = 1;
                fields.isOnline = 1;
            } else if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.ADMIN) {
                fields.role = 1;
            }

            const data = await UserModel.findById(Types.ObjectId(req.params._id), fields);
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC011", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC012", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getUserList(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "UC013", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            const condition: any = {};

            if (!Utils.isEmpty(req.params.userType)) {
                condition.userType = req.params.userType;
            }

            let fields: any = "userType firstName lastName gender mobileNumber emailId dob";
            if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.SELLER) {

                fields = fields + " isOnline";

                if (!Utils.isEmpty(req.body.language)) {
                    condition.languagesKnown = req.body.language;
                }

                if (!Utils.isEmpty(req.body.userCategoryType)) {
                    condition.userCategoryType = req.body.userCategoryType;
                }
            } else if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.ADMIN) {
                fields = "userType userName firstName lastName role status";
                condition.userName = { $regex: new RegExp(req.body.searchParam, "i") };
            }

            const options = {
                select: fields,
                sort: { createdAt: -1 },
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
            };
            const data: any = await UserModel.paginate(condition, options);

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC014", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC015", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getAllUserList(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "UC016", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            let data: any = {};
            const fields: any = { userName: 1 };
            const condition: any = { status: true, userType: { $ne: C.USER_TYPE.ADMIN } };

            if (!Utils.isEmpty(req.body.categoryId) || !Utils.isEmpty(req.body.subCategoryId)) {
                const sellerServicesCondition: any = {};

                if (!Utils.isEmpty(req.body.categoryId)) {
                    sellerServicesCondition.category = Types.ObjectId(req.body.categoryId);
                }

                if (!Utils.isEmpty(req.body.subCategoryId)) {
                    sellerServicesCondition.subCategory = Types.ObjectId(req.body.subCategoryId);
                }

                const distinctUserIds: any = await SellerServicesModel.distinct("userMappingKey", sellerServicesCondition);
                condition._id = { $in: distinctUserIds };
            }

            data = await UserModel.find(condition, fields);

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC017", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC018", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async login(req: Request, res: Response) {

        try {
            const user = await UserModel.findOne({ userName: req.body.userName, status: true });
            if (!user) {

                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC019", MSG.INVALID_USER_NAME_PASSWORD, MSG.INVALID_USER_NAME_PASSWORD, MSG.INVALID_USER_NAME_PASSWORD);
                return Promise.reject(error);
            }

            const isMatch = await user.comparePassword(req.body.password);
            const permissions = await C.aclObj.whatResources(user.role);
            if (isMatch) {
                if (Utils.isEmpty(permissions) && user.userType === C.USER_TYPE.ADMIN) {

                    const error = ResponseFormatter.getErrorResponseWithBody(500, "UC020", MSG.PERMISSION_ERROR, MSG.PERMISSION_ERROR, MSG.PERMISSION_ERROR);
                    return Promise.reject(error);
                } else {
                    const token = JwtTokenService.generateToken(user);
                    const params = {
                        token,
                        loginAt: moment().toDate(),
                    };

                    const data = await UserModel.findByIdAndUpdate(user._id, params, { new: true });
                    const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                    return Promise.resolve(success);
                }
            } else {

                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC021", MSG.INVALID_USER_NAME_PASSWORD, MSG.INVALID_USER_NAME_PASSWORD, MSG.INVALID_USER_NAME_PASSWORD);
                return Promise.reject(error);
            }
        } catch (Exception) {

            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC022", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async logout(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC023", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const updateData = {
                token: "",
                logoutAt: moment().toDate(),
            };

            const data = await UserModel.findByIdAndUpdate(req.params._id, updateData, { new: true });
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC024", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.LOGOUT);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC025", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async markingFavoriteServices(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC026", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            if (Utils.isEmpty(req.body.userId) || Utils.isEmpty(req.body.serviceId) || ["mark", "unmark"].indexOf(req.params.activityType) === -1) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC027", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            const userActivityFields: any = { favoriteServices: 1 };
            const condition: any = {
                userMappingKey: Types.ObjectId(req.body.userId),
            };
            const userActivity: any = await UserActivityModel.findOne(condition, userActivityFields);
            let data: any = {};
            if (Utils.isEmpty(userActivity) && req.params.activityType === "mark") {
                const userActivityDataObj: any = {
                    favoriteServices: [],
                    userMappingKey: Types.ObjectId(req.body.userId),
                };
                userActivityDataObj.favoriteServices.push(Types.ObjectId(req.body.serviceId));
                const userActivityModel = new UserActivityModel(userActivityDataObj);
                data = await userActivityModel.save();
            } else {
                if (req.params.activityType === "mark") {
                    if (!userActivity.favoriteServices.includes(Types.ObjectId(req.body.serviceId))) {
                        userActivity.favoriteServices.push(Types.ObjectId(req.body.serviceId));
                    }
                } else if (req.params.activityType === "unmark") {
                    const index = userActivity.favoriteServices.indexOf(Types.ObjectId(req.body.serviceId));
                    if (index > -1) {
                        userActivity.favoriteServices.splice(index, 1);
                    }
                }
                data = await UserActivityModel.findByIdAndUpdate(userActivity._id, userActivity, { new: true });
            }

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC028", MSG.RECORD_UPDATION_FAILED, MSG.RECORD_UPDATION_FAILED, MSG.RECORD_UPDATION_FAILED);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.RECORD_UPDATED_SUCCESSFULLY);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC029", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getFavoriteServiceList(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC030", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            if (Utils.isEmpty(req.body.userId)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC031", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            const fields: any = { favoriteServices: 1 };
            const condition: any = {
                userMappingKey: Types.ObjectId(req.body.userId),
            };
            const populateOptions = {
                path: "favoriteServices",
                select: "title imageList rating packages.packageType packages.price userMappingKey",
                populate: { path: "userMappingKey", select: "firstName lastName description image rating isOnline" },
                options: {
                    sort: { createdAt: -1 },
                    skip: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                    limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
                },
            };
            const userActivityModel: any = await UserActivityModel.findOne(condition, fields);
            const data: any = {
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
            };
            if (!Utils.isEmpty(userActivityModel)) {
                const userActivityModelData: any = Utils.getArrayCopy(userActivityModel);
                const userActivityData: any = await userActivityModel.populate(populateOptions)
                    .execPopulate();
                if (!Utils.isEmpty(userActivityData)) {
                    data.docs = !Utils.isEmpty(userActivityData.favoriteServices) ? userActivityData.favoriteServices : [];
                    data.total = !Utils.isEmpty(userActivityModelData.favoriteServices) ? userActivityModelData.favoriteServices.length : 0;
                } else {
                    data.docs = null;
                    data.total = 0;
                }
            } else {
                data.docs = null;
                data.total = 0;
            }

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC032", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC033", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getDashboardData(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC034", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            if (Utils.isEmpty(req.body.userId)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC035", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            if (!["recentSearchServices", "savedServices", "purchasedServices"].includes(req.body.serviceType)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC036", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            const fields: any = { favoriteServices: 1 };
            fields[req.body.serviceType] = 1;
            const condition: any = {
                userMappingKey: Types.ObjectId(req.body.userId),
            };
            const populateOptions = {
                path: req.body.serviceType,
                select: "title imageList rating packages.packageType packages.price userMappingKey",
                populate: { path: "userMappingKey", select: "firstName lastName description image rating isOnline" },
                options: {
                    sort: { createdAt: -1 },
                    skip: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                    limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
                    lean: true,
                },
            };
            const userActivityModel: any = await UserActivityModel.findOne(condition, fields);
            const data: any = {
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
            };

            let userActivityData: any = {};
            if (!Utils.isEmpty(userActivityModel)) {
                const userActivityModelData: any = Utils.getArrayCopy(userActivityModel);
                userActivityData = await userActivityModel.populate(populateOptions)
                    .execPopulate();
                if (!Utils.isEmpty(userActivityData)) {
                    data.docs = !Utils.isEmpty(userActivityData[req.body.serviceType]) ? userActivityData[req.body.serviceType] : [];
                    data.total = !Utils.isEmpty(userActivityModelData[req.body.serviceType]) ? userActivityModelData[req.body.serviceType].length : 0;
                } else {
                    data.docs = null;
                    data.total = 0;
                }
            } else {
                data.docs = null;
                data.total = 0;
            }
            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC037", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {

                const favoriteServices: any = !Utils.isEmpty(userActivityData.favoriteServices) ? userActivityData.favoriteServices : [];
                for (const sellerService of data.docs) {
                    if (favoriteServices.includes(sellerService._id)) {
                        sellerService.isFavorite = true;
                    } else {
                        sellerService.isFavorite = false;
                    }
                }
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC038", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }
    // public async getDashboardData(req: Request, res: Response) {

    //     try {

    //         const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
    //         if (Utils.isEmpty(authorizationResult)) {
    //             const error = ResponseFormatter.getErrorResponseWithBody(500, "UC039", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
    //             return Promise.reject(error);
    //         }

    //         if (Utils.isEmpty(req.body.userId)) {
    //             const error = ResponseFormatter.getErrorResponseWithBody(500, "UC040", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
    //             return Promise.reject(error);
    //         }

    //         const userActivityFields: any = { favoriteServices: 1 , recentSearchServices: 1, savedServices: 1, purchasedServices: 1, _id: 0 };
    //         const condition: any = {
    //             userMappingKey: Types.ObjectId(req.body.userId),
    //         };
    //         const data: any = await UserActivityModel.findOne(condition, userActivityFields)
    //         .populate({ path: "recentSearchServices", select: "title imageList rating packages.packageType packages.price" })
    //         .populate({ path: "savedServices", select: "title imageList rating packages.packageType packages.price" })
    //         .populate({ path: "purchasedServices", select: "title imageList rating packages.packageType packages.price" })
    //         .exec();
    //         if (Utils.isEmpty(data)) {
    //             const error = ResponseFormatter.getErrorResponseWithBody(500, "UC041", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
    //             return Promise.reject(error);
    //         } else {
    //             const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
    //             return Promise.resolve(success);
    //         }

    //     } catch (Exception) {

    //         console.log(Exception);
    //         const error = ResponseFormatter.getErrorResponseWithBody(500, "UC042", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
    //         return Promise.reject(error);
    //     }
    // }
}
