import { Request, Response, Router } from "express";

import { Utils } from "../../lib/Utils";
import { C } from "../../config/constants/constants";
import { JwtTokenService } from "../services/JwtTokenService";
import moment = require("moment-timezone");
moment.tz.setDefault(C.TIMEZONE);
import { ResponseFormatter } from "../../lib/ResponseFormatter";
import { MessagesConstants as MSG } from "../../config/constants/messages.constants";
import { Types } from "mongoose";

import { CategoryModel } from "../models/CategoryModel";
import { SubCategoryModel } from "../models/SubCategoryModel";

export class CategoryController {

    constructor() {

    }

    public async insertCategoryDetails(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC001", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const categoryData: any = {};
            const subCategoryMappingKey: any = [];
            if (!Utils.isEmpty(req.body.subCategoryMappingKey) && Array.isArray(req.body.subCategoryMappingKey)) {
                for (const subCategoryId of req.body.subCategoryMappingKey) {
                    subCategoryMappingKey.push(Types.ObjectId(subCategoryId));
                }
            }
            categoryData.subCategoryMappingKey = subCategoryMappingKey;

            if (!Utils.isEmpty(req.body.name)) {
                categoryData.name = req.body.name;
            }

            if (!Utils.isEmpty(req.body.description)) {
                categoryData.description = req.body.description;
            }

            if (Utils.isSet(req.body.status)) {
                categoryData.status = (/true/i).test(req.body.status);
            }

            if (Utils.isSet(req.body.isPopularCategory)) {
                categoryData.isPopularCategory = (/true/i).test(req.body.isPopularCategory);
            }

            const category = new CategoryModel(categoryData);

            const data = await await category.save();

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC002", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC003", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async updateCategoryDetails(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC004", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const categoryData: any = {};
            if (!Utils.isEmpty(req.body.subCategoryMappingKey) && Array.isArray(req.body.subCategoryMappingKey)) {
                const subCategoryMappingKey: any = [];
                for (const subCategoryId of req.body.subCategoryMappingKey) {
                    subCategoryMappingKey.push(Types.ObjectId(subCategoryId));
                }
                categoryData.subCategoryMappingKey = subCategoryMappingKey;
            }

            if (!Utils.isEmpty(req.body.name)) {
                categoryData.name = req.body.name;
            }

            if (!Utils.isEmpty(req.body.description)) {
                categoryData.description = req.body.description;
            }

            if (Utils.isSet(req.body.status)) {
                categoryData.status = (/true/i).test(req.body.status);
            }

            if (Utils.isSet(req.body.isPopularCategory)) {
                categoryData.isPopularCategory = (/true/i).test(req.body.isPopularCategory);
            }

            const data = await CategoryModel.findByIdAndUpdate(req.params._id, categoryData, { new: true });

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC005", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC006", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getCategoryList(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC007", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            let fields: any = "name description image";
            const condition: any = {};
            let data: any = {};
            if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.ADMIN) {
                fields = fields + " status";
                condition.name = { $regex: new RegExp(req.body.searchParam, "i") };
            } else {
                condition.status = true;
            }

            const options = {
                select: fields,
                sort: { createdAt: -1 },
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
            };
            data = await CategoryModel.paginate(condition, options);

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC008", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC009", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getAllCategoryList(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC010", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            let data: any = {};
            if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.ADMIN) {
                const condition: any = { status: true };
                const fields: any = { name: 1 };
                data = await CategoryModel.find(condition, fields);
            }

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC011", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC012", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getCategoryDetails(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC013", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = { name: 1, description: 1, image: 1, isPopularCategory: 1, status: 1, subCategoryMappingKey: 1 };
            const data: any = await CategoryModel.findById(Types.ObjectId(req.body.categoryId), fields);
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC014", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC015", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async insertSubCategoryDetails(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC016", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const subCategoryDataObj: any = {};
            if (!Utils.isEmpty(req.body.name)) {
                subCategoryDataObj.name = req.body.name;
            }

            if (!Utils.isEmpty(req.body.description)) {
                subCategoryDataObj.description = req.body.description;
            }

            if (!Utils.isEmpty(req.body.groupName)) {
                subCategoryDataObj.groupName = req.body.groupName;
            }

            let serviceTypeList = [];
            if (!Utils.isEmpty(req.body.serviceTypes)) {
                if (Array.isArray(req.body.serviceTypes)) {
                    serviceTypeList = req.body.serviceTypes;
                } else {
                    serviceTypeList.push(req.body.serviceTypes);
                }
            }
            subCategoryDataObj.serviceTypes = serviceTypeList;

            let platformsList = [];
            if (!Utils.isEmpty(req.body.platforms)) {
                if (Array.isArray(req.body.platforms)) {
                    platformsList = req.body.platforms;
                } else {
                    platformsList.push(req.body.platforms);
                }
            }
            subCategoryDataObj.platforms = platformsList;

            let deliveryTimeList = [];
            if (!Utils.isEmpty(req.body.deliveryTime)) {
                if (Array.isArray(req.body.deliveryTime)) {
                    deliveryTimeList = req.body.deliveryTime;
                } else {
                    deliveryTimeList.push(req.body.deliveryTime);
                }
            }
            subCategoryDataObj.deliveryTime = deliveryTimeList;

            let revisionsList = [];
            if (!Utils.isEmpty(req.body.revisions)) {
                if (Array.isArray(req.body.revisions)) {
                    revisionsList = req.body.revisions;
                } else {
                    revisionsList.push(req.body.revisions);
                }
            }
            subCategoryDataObj.revisions = revisionsList;

            const packagesObj: any = [];
            if (!Utils.isEmpty(req.body.packages)) {
                for (const packageData of req.body.packages) {
                    const packageObj: any = {
                        packageType: !Utils.isEmpty(packageData.packageType) ? packageData.packageType : "",
                        services: [],
                    };
                    if (!Utils.isEmpty(packageData.minPrice)) {
                        packageObj.minPrice = packageData.minPrice;
                    }
                    if (!Utils.isEmpty(packageData.maxPrice)) {
                        packageObj.maxPrice = packageData.maxPrice;
                    }
                    if (!Utils.isEmpty(packageData.currencyType)) {
                        packageObj.currencyType = packageData.currencyType;
                    }
                    if (!Utils.isEmpty(packageData.services)) {
                        if (Array.isArray(packageData.services)) {
                            packageObj.services = packageData.services;
                        } else {
                            packageObj.services.push(packageData.services);
                        }
                    }
                    packagesObj.push(packageObj);
                }
            }
            subCategoryDataObj.packages = packagesObj;

            if (Utils.isSet(req.body.status)) {
                subCategoryDataObj.status = (/true/i).test(req.body.status);
            }

            const subCategory = new SubCategoryModel(subCategoryDataObj);
            const data = await subCategory.save();
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC017", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC018", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async updateSubCategoryDetails(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC019", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const subCategoryData: any = {};
            if (!Utils.isEmpty(req.body.name)) {
                subCategoryData.name = req.body.name;
            }

            if (!Utils.isEmpty(req.body.description)) {
                subCategoryData.description = req.body.description;
            }

            if (!Utils.isEmpty(req.body.groupName)) {
                subCategoryData.groupName = req.body.groupName;
            }

            if (!Utils.isEmpty(req.body.serviceTypes)) {
                let serviceTypes: any = [];
                if (Array.isArray(req.body.serviceTypes)) {
                    serviceTypes = req.body.serviceTypes;
                } else {
                    serviceTypes.push(req.body.serviceTypes);
                }
                subCategoryData.serviceTypes = serviceTypes;
            }

            if (!Utils.isEmpty(req.body.platforms)) {
                let platforms: any = [];
                if (Array.isArray(req.body.platforms)) {
                    platforms = req.body.platforms;
                } else {
                    platforms.push(req.body.platforms);
                }
                subCategoryData.platforms = platforms;
            }

            if (!Utils.isEmpty(req.body.deliveryTime)) {
                let deliveryTimeList = [];
                if (Array.isArray(req.body.deliveryTime)) {
                    deliveryTimeList = req.body.deliveryTime;
                } else {
                    deliveryTimeList.push(req.body.deliveryTime);
                }
                subCategoryData.deliveryTime = deliveryTimeList;
            }

            if (!Utils.isEmpty(req.body.revisions)) {
                let revisionsList = [];
                if (Array.isArray(req.body.revisions)) {
                    revisionsList = req.body.revisions;
                } else {
                    revisionsList.push(req.body.revisions);
                }
                subCategoryData.revisions = revisionsList;
            }

            if (!Utils.isEmpty(req.body.packages)) {
                const packages: any = [];
                for (const packageData of req.body.packages) {
                    const packageObj: any = {
                        packageType: !Utils.isEmpty(packageData.packageType) ? packageData.packageType : "",
                        services: [],
                    };
                    if (!Utils.isEmpty(packageData.minPrice)) {
                        packageObj.minPrice = packageData.minPrice;
                    }
                    if (!Utils.isEmpty(packageData.maxPrice)) {
                        packageObj.maxPrice = packageData.maxPrice;
                    }
                    if (!Utils.isEmpty(packageData.currencyType)) {
                        packageObj.currencyType = packageData.currencyType;
                    }
                    if (!Utils.isEmpty(packageData.services)) {
                        let services: any = [];
                        if (Array.isArray(packageData.services)) {
                            services = packageData.services;
                        } else {
                            services.push(packageData.services);
                        }
                        packageObj.services = services;
                    }
                    packages.push(packageObj);
                }
                subCategoryData.packages = packages;
            }

            if (Utils.isSet(req.body.status)) {
                subCategoryData.status = (/true/i).test(req.body.status);
            }

            const data = await SubCategoryModel.findByIdAndUpdate(req.params._id, subCategoryData, { new: true });

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC020", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC021", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getSubCategoryList(req: Request, res: Response) {

        try {
            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC022", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            let data: any = {};
            if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.ADMIN) {
                const condition: any = {};
                condition.name = { $regex: new RegExp(req.body.searchParam, "i") };
                const options = {
                    select: "name description serviceTypes platforms packages status",
                    sort: { createdAt: -1 },
                    offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                    limit: req.body.limit,
                };
                data = await SubCategoryModel.paginate(condition, options);
            } else {
                const fields: any = { subCategoryMappingKey: 1 };
                const populateOptions = {
                    path: "subCategoryMappingKey",
                    select: "name description groupName",
                    options: {
                        sort: { createdAt: -1 },
                        skip: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                        limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
                    },
                };
                const categoryModel: any = await CategoryModel.findById(Types.ObjectId(req.body.categoryId), fields);
                data.offset = Utils.isEmpty(req.body.skip) ? 0 : req.body.skip;
                data.limit = Utils.isEmpty(req.body.limit) ? 10 : req.body.limit;
                if (!Utils.isEmpty(categoryModel)) {
                    const categoryModelData: any = Utils.getArrayCopy(categoryModel);
                    const categoryData: any = await categoryModel.populate(populateOptions)
                        .execPopulate();
                    if (!Utils.isEmpty(categoryData)) {
                        data.docs = !Utils.isEmpty(categoryData.subCategoryMappingKey) ? categoryData.subCategoryMappingKey : [];
                        data.total = !Utils.isEmpty(categoryModelData.subCategoryMappingKey) ? categoryModelData.subCategoryMappingKey.length : 0;
                    } else {
                        data.docs = null;
                        data.total = 0;
                    }
                } else {
                    data.docs = null;
                    data.total = 0;
                }
            }

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC023", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC024", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getAllSubCategoryList(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC025", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            let data: any = [];
            if (!Utils.isEmpty(req.body.categoryId)) {
                const categoryModel: any = await CategoryModel.findById(Types.ObjectId(req.body.categoryId), { subCategoryMappingKey: 1, _id: 0 })
                    .populate({ path: "subCategoryMappingKey", select: "name", match: { status: true } })
                    .exec();

                if (!Utils.isEmpty(categoryModel)) {
                    data = categoryModel.subCategoryMappingKey;
                }

            } else {
                const fields: any = { name: 1 };
                const condition: any = { status: true };
                data = await SubCategoryModel.find(condition, fields);
            }

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC026", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC027", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getSubCategoryDetails(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC028", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = { serviceTypes: 1, platforms: 1, deliveryTime: 1, revisions: 1, packages: 1 };
            if (!Utils.isEmpty(req.params.userType) && req.params.userType === C.USER_TYPE.ADMIN) {
                fields.name = 1;
                fields.description = 1;
                fields.groupName = 1;
                fields.status = 1;
            }
            const data: any = await SubCategoryModel.findById(Types.ObjectId(req.body.subCategoryId), fields);
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC029", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC030", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }
}
