import { Request, Response, Router } from "express";

import { Utils } from "../../lib/Utils";
import { C } from "../../config/constants/constants";
import moment = require("moment-timezone");
moment.tz.setDefault(C.TIMEZONE);
import { ResponseFormatter } from "../../lib/ResponseFormatter";
import { MessagesConstants as MSG } from "../../config/constants/messages.constants";
import { JwtTokenService } from "../services/JwtTokenService";

import { EncryptedDataModel } from "../models/EncryptedDataModel";
import { EncryptionService } from "../services/EncryptionService";

export class EncryptionController {

    constructor() {

    }

    public async encrypt(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "EC001", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const encryptedIdList: any = [];
            if (Array.isArray(req.body)) {
                for (const data of req.body) {
                    const encrypted = EncryptionService.encrypt(JSON.stringify(data), C.ENC_ALGORITHM, C.ENC_PASS_PHRASE);
                    const query = { value: encrypted };
                    const update = { value: encrypted, updated: moment().toDate() };
                    const options = { upsert: true, new: true };

                    const encryptedData = await EncryptedDataModel.findOneAndUpdate(query, { $set: update, $setOnInsert: { added: moment().toDate() } }, options);
                    if (!encryptedData) {
                        encryptedIdList.push("");
                    } else {
                        encryptedIdList.push(encryptedData._id);
                    }
                }
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "EC002", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            return Promise.resolve(encryptedIdList);
        } catch (exception) {
            const error = ResponseFormatter.getErrorResponseWithBody(500, "EC003", MSG.SOMETHING_WENT_WRONG, MSG.SOMETHING_WENT_WRONG, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async decrypt(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "EC004", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const decryptedDataList: any = [];
            if (Array.isArray(req.body)) {
                for (const data of req.body) {
                    const encryptedData = await EncryptedDataModel.findById(data.id);
                    if (!encryptedData) {
                        decryptedDataList.push({});
                    } else {
                        const decryptedData = JSON.parse(EncryptionService.decrypt(encryptedData.value, C.ENC_ALGORITHM, C.ENC_PASS_PHRASE));
                        decryptedDataList.push(decryptedData);
                    }
                }
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "EC005", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }
            return Promise.resolve(decryptedDataList);
        } catch (exception) {
            const error = ResponseFormatter.getErrorResponseWithBody(500, "EC006", MSG.SOMETHING_WENT_WRONG, MSG.SOMETHING_WENT_WRONG, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async search(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "EC007", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const encrypted = EncryptionService.encrypt(JSON.stringify(req.body), C.ENC_ALGORITHM, C.ENC_PASS_PHRASE);
            const encryptedData = await EncryptedDataModel.findOne({ value: encrypted });
            if (!encryptedData) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "EC008", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            }

            return Promise.resolve(encryptedData._id);
        } catch (exception) {
            const error = ResponseFormatter.getErrorResponseWithBody(500, "EC009", MSG.SOMETHING_WENT_WRONG, MSG.SOMETHING_WENT_WRONG, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }
}
