import { Request, Response, Router } from "express";

import { Utils } from "../../lib/Utils";
import { C } from "../../config/constants/constants";
import { JwtTokenService } from "../services/JwtTokenService";
import { NotificationService } from "../services/NotificationService";
import moment = require("moment-timezone");
moment.tz.setDefault(C.TIMEZONE);
import { ResponseFormatter } from "../../lib/ResponseFormatter";
import { MessagesConstants as MSG } from "../../config/constants/messages.constants";
import { Types } from "mongoose";

import { SellerServicesModel } from "../models/SellerServicesModel";
import { UserModel } from "../models/UserModel";
import { UserActivityModel } from "../models/UserActivityModel";
import { PostServiceRequestModel } from "../models/PostServiceRequestModel";

export class SellerProfileController {

    constructor() {

    }

    public async createSellerService(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC001", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = { userType: 1, status: 1 };
            const user: any = await UserModel.findById(Types.ObjectId(req.body.userId), fields);

            if (Utils.isEmpty(user) || (user.status === false) || (user.userType !== C.USER_TYPE.SELLER)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC002", MSG.INVALID_USER_PROFILE, MSG.INVALID_USER_PROFILE, MSG.INVALID_USER_PROFILE);
                return Promise.reject(error);
            }

            const sellerServicesObj: any = {
                userMappingKey: Types.ObjectId(req.body.userId),
            };

            if (!Utils.isEmpty(req.body.title)) {
                sellerServicesObj.title = req.body.title;
            }

            if (!Utils.isEmpty(req.body.category)) {
                sellerServicesObj.category = Types.ObjectId(req.body.category);
            }

            if (!Utils.isEmpty(req.body.subCategory)) {
                sellerServicesObj.subCategory = Types.ObjectId(req.body.subCategory);
            }

            if (!Utils.isEmpty(req.body.serviceType)) {
                sellerServicesObj.serviceType = req.body.serviceType;
            }

            if (!Utils.isEmpty(req.body.platform)) {
                sellerServicesObj.platform = req.body.platform;
            }

            if (!Utils.isEmpty(req.body.expertise)) {
                sellerServicesObj.expertise = req.body.expertise;
            }

            if (!Utils.isEmpty(req.body.description)) {
                sellerServicesObj.description = req.body.description;
            }

            if (!Utils.isEmpty(req.body.requirementDetails)) {
                sellerServicesObj.requirementDetails = req.body.requirementDetails;
            }

            const packagesObj: any = [];
            if (!Utils.isEmpty(req.body.packages)) {
                for (const packageData of req.body.packages) {
                    const packageObj = {
                        packageType: !Utils.isEmpty(packageData.packageType) ? packageData.packageType : "",
                        packageName: !Utils.isEmpty(packageData.packageName) ? packageData.packageName : "",
                        packageDescription: !Utils.isEmpty(packageData.packageDescription) ? packageData.packageDescription : "",
                        deliveryTime: !Utils.isEmpty(packageData.deliveryTime) ? packageData.deliveryTime : "",
                        mobileOS: !Utils.isEmpty(packageData.mobileOS) ? packageData.mobileOS : "",
                        revisions: !Utils.isEmpty(packageData.revisions) ? packageData.revisions : 0,
                        price: !Utils.isEmpty(packageData.price) ? packageData.price : 0,
                        // minPrice: !Utils.isEmpty(packageData.minPrice) ? packageData.minPrice : 0,
                        // maxPrice: !Utils.isEmpty(packageData.maxPrice) ? packageData.maxPrice : 0,
                        currencyType: !Utils.isEmpty(packageData.currencyType) ? packageData.currencyType : "",
                        services: !Utils.isEmpty(packageData.services) ? packageData.services : [],
                        extraServices: [],
                    };

                    const extraServicesObj: any = [];
                    if (!Utils.isEmpty(packageData.extraServices)) {
                        for (const extraServiceData of packageData.extraServices) {
                            extraServicesObj.push({
                                serviceName: !Utils.isEmpty(extraServiceData.serviceName) ? extraServiceData.serviceName : "",
                                revisions: !Utils.isEmpty(extraServiceData.revisions) ? extraServiceData.revisions : 0,
                                price: !Utils.isEmpty(extraServiceData.price) ? extraServiceData.price : 0,
                                // minPrice: !Utils.isEmpty(extraServiceData.minPrice) ? extraServiceData.minPrice : 0,
                                // maxPrice: !Utils.isEmpty(extraServiceData.maxPrice) ? extraServiceData.maxPrice : 0,
                                currencyType: !Utils.isEmpty(extraServiceData.currencyType) ? extraServiceData.currencyType : "",
                            });
                        }
                    }
                    packageObj.extraServices = extraServicesObj;
                    packagesObj.push(packageObj);
                }
            }
            sellerServicesObj.packages = packagesObj;

            const faqObj: any = [];
            if (!Utils.isEmpty(req.body.faq)) {
                for (const faqData of req.body.faq) {
                    faqObj.push({
                        question: !Utils.isEmpty(faqData.question) ? faqData.question : "",
                        answer: !Utils.isEmpty(faqData.answer) ? faqData.answer : "",
                    });
                }
            }
            sellerServicesObj.faq = faqObj;

            const sellerService = new SellerServicesModel(sellerServicesObj);

            const data = await sellerService.save();
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC003", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC004", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getSellerServiceList(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC005", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            const promiseArray: any = [];
            const condition: any = { status: true };
            if (!Utils.isEmpty(req.body.sellerId)) {
                condition.userMappingKey = Types.ObjectId(req.body.sellerId);
            }

            if (!Utils.isEmpty(req.body.subCategoryId)) {
                condition.subCategory = Types.ObjectId(req.body.subCategoryId);
            }

            const fields: any = "title imageList rating packages.packageType packages.price";
            const options = {
                select: fields,
                sort: { createdAt: -1 },
                populate: { path: "userMappingKey", select: "firstName lastName description image rating isOnline" },
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
                lean: true,
                leanWithId: false,
            };
            promiseArray.push(SellerServicesModel.paginate(condition, options));

            if (!Utils.isEmpty(req.headers.authorization)) {
                const user: any = await UserModel.findOne({ token: req.headers.authorization }, { _id: 1 });
                const userActivityFields: any = { favoriteServices: 1 };
                const conditionForuserActivityData: any = {
                    userMappingKey: user._id,
                };
                promiseArray.push(UserActivityModel.findOne(conditionForuserActivityData, userActivityFields));
            }

            const results: any = await Promise.all(promiseArray);
            const data: any = results[0];

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC006", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                if (!Utils.isEmpty(req.headers.authorization)) {
                    const favoriteServices: any = !Utils.isEmpty(results[1]) ? results[1].favoriteServices : [];
                    for (const sellerService of data.docs) {
                        if (favoriteServices.includes(sellerService._id)) {
                            sellerService.isFavorite = true;
                        } else {
                            sellerService.isFavorite = false;
                        }
                    }
                }

                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC007", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getAllSellerServiceList(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC008", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            const condition: any = { status: true };

            if (!Utils.isEmpty(req.body.categoryId)) {
                condition.category = req.body.categoryId;
            }

            if (!Utils.isEmpty(req.body.subCategoryId)) {
                condition.subCategory = req.body.subCategoryId;
            }

            if (!Utils.isEmpty(req.body.sellerId)) {
                condition.userMappingKey = req.body.sellerId;
            }

            if (!Utils.isEmpty(req.body.offerId)) {
                condition.offers = req.body.offerId ;
            }

            const fields: any = { title: 1 };
            const data: any = await SellerServicesModel.find(condition, fields);

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC009", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC010", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getSellerServiceDetails(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC011", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            const fields: any = { "status": 0, "packages._id": 0, "packages.extraServices._id": 0, "faq._id": 0, "updatedAt": 0, "createdAt": 0 };
            const currentDateTime = moment().toDate();
            const data: any = await SellerServicesModel.findById(Types.ObjectId(req.body.sellerServiceId), fields)
                .populate({ path: "offers", select: "title discountPercentage", match: { validFrom: { $lte: currentDateTime }, validTo: { $gte: currentDateTime }, status: true } })
                .populate({ path: "userMappingKey", select: "firstName lastName description image rating isOnline" })
                .exec();

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC012", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                if (!Utils.isEmpty(req.headers.authorization)) {
                    const user: any = await UserModel.findOne({ token: req.headers.authorization }, { _id: 1 });
                    await this.updateUserActivityServices(user.id, data.id, "recentSearchServices");
                }
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC013", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async searchServices(req: Request, res: Response) {

        try {

            if (!Utils.isEmpty(req.headers.authorization)) {
                const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
                if (Utils.isEmpty(authorizationResult)) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC014", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                    return Promise.reject(error);
                }
            }

            if (Utils.isEmpty(req.body.searchData)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC015", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            // const condition: any = { status: true };
            // const userActivityFields: any = { title: 1, subCategory: 1 };
            // const dataObj: any = await SellerServicesModel.find(condition, userActivityFields)
            //     .populate({ path: "subCategory", select: "name", match: { name: { $regex: new RegExp(req.body.searchData, "i") } } })
            //     .exec();

            // const data: any = [];
            // if (!Utils.isEmpty(dataObj)) {
            //     for (const obj of dataObj) {
            //         if (!Utils.isEmpty(obj.subCategory)) {
            //             data.push(obj);
            //         }
            //     }
            // }

            // if (Utils.isEmpty(data)) {
            //     const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC013", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
            //     return Promise.reject(error);
            // } else {
            //     const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
            //     return Promise.resolve(success);
            // }

            const promiseArray: any = [];
            const condition: any = {
                "status": true,
                "packages.services": { $regex: new RegExp(req.body.searchData, "i") },
            };
            const fields: any = "title imageList rating packages.packageType packages.price";
            const options = {
                select: fields,
                sort: { createdAt: -1 },
                populate: { path: "userMappingKey", select: "firstName lastName description image rating isOnline" },
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
                lean: true,
                leanWithId: false,
            };
            promiseArray.push(SellerServicesModel.paginate(condition, options));

            if (!Utils.isEmpty(req.headers.authorization)) {
                const user: any = await UserModel.findOne({ token: req.headers.authorization }, { _id: 1 });
                const userActivityFields: any = { favoriteServices: 1 };
                const conditionForuserActivityData: any = {
                    userMappingKey: user._id,
                };
                promiseArray.push(UserActivityModel.findOne(conditionForuserActivityData, userActivityFields));
            }

            const results: any = await Promise.all(promiseArray);
            const data: any = results[0];

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC016", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                if (!Utils.isEmpty(req.headers.authorization)) {
                    const favoriteServices: any = !Utils.isEmpty(results[1]) ? results[1].favoriteServices : [];
                    for (const sellerService of data.docs) {
                        if (favoriteServices.includes(sellerService._id)) {
                            sellerService.isFavorite = true;
                        } else {
                            sellerService.isFavorite = false;
                        }
                    }
                }

                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC017", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async createPostServiceRequest(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC018", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const postServiceRequestDataObj: any = {};
            if (!Utils.isEmpty(req.body.description)) {
                postServiceRequestDataObj.description = req.body.description;
            }

            if (!Utils.isEmpty(req.body.category)) {
                postServiceRequestDataObj.category = Types.ObjectId(req.body.category);
            }

            if (!Utils.isEmpty(req.body.subCategory)) {
                postServiceRequestDataObj.subCategory = Types.ObjectId(req.body.subCategory);
            }

            if (!Utils.isEmpty(req.body.deliveryTime)) {
                postServiceRequestDataObj.deliveryTime = req.body.deliveryTime;
            }

            if (!Utils.isEmpty(req.body.price)) {
                postServiceRequestDataObj.price = req.body.price;
            }

            if (!Utils.isEmpty(req.body.currencyType)) {
                postServiceRequestDataObj.currencyType = req.body.currencyType;
            }

            const postServiceRequestModel = new PostServiceRequestModel(postServiceRequestDataObj);
            const data = await postServiceRequestModel.save();

            ////////////// Notification //////////////
            try {
                const sellerServicesCondition: any = {
                    category: Types.ObjectId(req.body.category),
                    subCategory: Types.ObjectId(req.body.subCategory),
                };
                const distinctUserIds: any = await SellerServicesModel.distinct("userMappingKey", sellerServicesCondition);

                const fields: any = { userType: 1, notificationToken: 1 };
                const condition: any = {
                    _id: { $in: distinctUserIds },
                    notificationToken: { $ne: null },
                    userType: "Seller",
                    havingSellerPermission: true,
                    status: true,
                };
                const userData = await UserModel.find(condition, fields);

                if (!Utils.isEmpty(userData)) {
                    const notificationData: any = {
                        title: "Service Request",
                        body: "User requested for specific service.",
                    };

                    const notificationResponseData: any = await NotificationService.sendNotification(userData, notificationData);
                }
            } catch (Exception) {
                console.log(Exception);
            }
            ////////////// Notification //////////////

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC019", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC020", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async updatepPostServiceRequestAcceptDetails(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC021", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const user: any = await UserModel.findOne({ token: req.headers.authorization }, { _id: 1 });

            const fields: any = { offerAcceptList: 1 };
            const getPostServiceRequestData: any = await PostServiceRequestModel.findById(Types.ObjectId(req.params._id), fields);

            const postServiceRequestData: any = {
                userMappingKey: user._id,
            };

            if (!Utils.isEmpty(req.body.description)) {
                postServiceRequestData.description = req.body.description;
            }

            if (!Utils.isEmpty(req.body.deliveryTime)) {
                postServiceRequestData.deliveryTime = req.body.deliveryTime;
            }

            if (Utils.isEmpty(getPostServiceRequestData.offerAcceptList)) {
                getPostServiceRequestData.offerAcceptList = [postServiceRequestData];
            } else {
                getPostServiceRequestData.offerAcceptList.push(postServiceRequestData);
            }

            const data = await PostServiceRequestModel.findByIdAndUpdate(req.params._id, getPostServiceRequestData, { new: true });

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC022", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC023", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getPostServiceRequestList(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC024", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const sellerServicesCondition: any = {
                userMappingKey: Types.ObjectId(req.body.sellerId),
            };
            const distinctSubCategoryIds: any = await SellerServicesModel.distinct("subCategory", sellerServicesCondition);

            const fields: any = "description";
            const condition: any = {
                subCategory: { $in: distinctSubCategoryIds },
            };

            const options = {
                select: fields,
                sort: { createdAt: -1 },
                lean: true,
                leanWithId: false,
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
            };
            const data: any = await PostServiceRequestModel.paginate(condition, options);

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC025", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC026", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getPostServiceRequestDetails(req: Request, res: Response) {

        try {
            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC027", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = { "imageList": 0, "offerAcceptList._id": 0, "status": 0, "createdAt": 0, "updatedAt": 0 };
            const data: any = await PostServiceRequestModel.findById(Types.ObjectId(req.body.postServiceRequestId), fields).lean()
                .populate({ path: "category", select: "name" })
                .populate({ path: "subCategory", select: "name" })
                .populate({ path: "offerAcceptList.userMappingKey", select: "userName firstName lastName" })
                .exec();
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC028", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                if (!Utils.isEmpty(data.offerAcceptList)) {
                    for (const offerAcceptData of data.offerAcceptList) {
                        offerAcceptData.createdAt = Utils.convertDateTimeFormatFromDB(offerAcceptData.createdAt);
                    }
                }
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC029", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    private async updateUserActivityServices(userId: string, serviceId: string, activityType: string) {

        try {
            const userActivityFields: any = {};
            userActivityFields[activityType] = 1;
            const condition: any = {
                userMappingKey: Types.ObjectId(userId),
            };
            const userActivity: any = await UserActivityModel.findOne(condition, userActivityFields);
            let data: any = {};
            if (Utils.isEmpty(userActivity)) {
                const userActivityDataObj: any = {
                    userMappingKey: Types.ObjectId(userId),
                };
                userActivityDataObj[activityType] = [];
                userActivityDataObj[activityType].push(Types.ObjectId(serviceId));
                const userActivityModel = new UserActivityModel(userActivityDataObj);
                data = await userActivityModel.save();
            } else {
                const index = userActivity[activityType].indexOf(Types.ObjectId(serviceId));
                if (index > -1) {
                    userActivity[activityType].splice(index, 1);
                }
                userActivity[activityType].push(Types.ObjectId(serviceId));
                data = await UserActivityModel.findByIdAndUpdate(userActivity._id, userActivity, { new: true });
            }
            return Promise.resolve(data);
        } catch (Exception) {
            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "SPC030", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }
}
