import { Request, Response, Router } from "express";

import { Utils } from "../../lib/Utils";
import { C } from "../../config/constants/constants";
import { JwtTokenService } from "../services/JwtTokenService";
import { SmsAndMailService } from "../services/SmsAndMailService";
import { NotificationService } from "../services/NotificationService";
import moment = require("moment-timezone");
moment.tz.setDefault(C.TIMEZONE);
import { ResponseFormatter } from "../../lib/ResponseFormatter";
import { MessagesConstants as MSG } from "../../config/constants/messages.constants";
import { Types } from "mongoose";

import { CategoryModel } from "../models/CategoryModel";
import { SellerServicesModel } from "../models/SellerServicesModel";
import { SmsAndMailModel } from "../models/SmsAndMailModel";
import { UserModel } from "../models/UserModel";
import { ConfigModel } from "../models/ConfigModel";
import { NotificationModel } from "../models/NotificationModel";
import { OffersModel } from "../models/Offers";

import ejs from "ejs";
import * as fs from "fs";

export class CommonController {

    constructor() {

    }

    public async sendOtp(req: Request, res: Response) {

        try {

            if (Utils.isEmpty(req.body.mobileNumber) && Utils.isEmpty(req.body.emailId)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC001", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            const finalResponse: any = {
                otpSendInMobile: false,
                otpSendInMail: false,
            };

            if (!Utils.isEmpty(req.body.mobileNumber)) {
                const otpNumber: string = await this.generateOtp(C.OTP_SOURCES.SMS, req.body.mobileNumber);
                const otp: any = {
                    source: req.body.mobileNumber,
                    otpCode: otpNumber,
                    message: `Your One Time Password is ${otpNumber}. It is active for 15 minutes only.`,
                };
                const condition: any = { status: true, configName: "smsConfig" };
                const fields: any = { configValue: 1, _id: 0 };
                let configData: any = await ConfigModel.findOne(condition, fields);
                if (!Utils.isEmpty(configData)) {
                    configData = configData.configValue[0];
                    const data: any = await SmsAndMailService.sendSms(otp.message, otp.source, configData);
                    const SmsAndMail = new SmsAndMailModel({
                        source: C.OTP_SOURCES.SMS,
                        from: configData.sendingFrom,
                        to: otp.source,
                        otp: otp.otpCode,
                        body: otp.message,
                        sendStatus: data.status,
                        sourceServerResponse: data.response,
                    });
                    const logData = await SmsAndMail.save();
                    if (!Utils.isEmpty(data.status)) {
                        finalResponse.otpSendInMobile = true;
                    } else {
                        finalResponse.otpSendInMobile = false;
                    }
                } else {
                    finalResponse.otpSendInMobile = false;
                }
            } else {
                finalResponse.otpSendInMobile = false;
            }

            if (!Utils.isEmpty(req.body.emailId) && req.body.emailId.includes("@")) {
                const otpNumber: string = await this.generateOtp(C.OTP_SOURCES.MAIL, req.body.emailId);
                const params = {
                    otpCode: otpNumber,
                    emailId: req.body.emailId,
                };
                const htmlFile = __dirname + "/../../../views/emailTemplate/otpVerification.ejs";
                const compiled = ejs.compile(fs.readFileSync(htmlFile, "utf8"));
                const mailBody = compiled(params);
                const mailSubject = "Please Verify Your Mail ID";
                const condition: any = { status: true, configName: "mailConfig" };
                const fields: any = { configValue: 1, _id: 0 };
                let configData: any = await ConfigModel.findOne(condition, fields);
                if (!Utils.isEmpty(configData)) {
                    configData = configData.configValue[0];
                    const data: any = await SmsAndMailService.sendMail(mailSubject, mailBody, req.body.emailId, configData);
                    const SmsAndMail = new SmsAndMailModel({
                        source: C.OTP_SOURCES.MAIL,
                        from: configData.sendingFrom,
                        to: req.body.emailId,
                        otp: otpNumber,
                        body: mailBody,
                        sendStatus: data.status,
                        sourceServerResponse: data.response,
                    });
                    const logData = await SmsAndMail.save();
                    if (!Utils.isEmpty(data.status)) {
                        finalResponse.otpSendInMail = true;
                    } else {
                        finalResponse.otpSendInMail = false;
                    }
                } else {
                    finalResponse.otpSendInMail = false;
                }
            } else {
                finalResponse.otpSendInMail = false;
            }

            if (finalResponse.otpSendInMobile || finalResponse.otpSendInMail) {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.OTP_SEND, finalResponse);
                return Promise.resolve(success);
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC002", MSG.OTP_SENDING_FAILED, MSG.OTP_SENDING_FAILED, MSG.OTP_SENDING_FAILED);
                return Promise.reject(error);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC003", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async verifyOtp(req: Request, res: Response) {

        try {
            if (Utils.isEmpty(req.body.otpCode)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC004", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            const previousDateTime = moment().add(-15, "minutes").toDate();
            const currentDateTime = moment().toDate();
            const condition: any = {
                sendStatus: true,
                otp: req.body.otpCode,
                createdAt: { $gte: previousDateTime, $lte: currentDateTime },
            };
            const fields: any = { source: 1, to: 1, _id: 0 };
            const data: any = await SmsAndMailModel.findOne(condition, fields);

            if (!Utils.isEmpty(data)) {
                const finalResponse: any = {
                    mobileNumber: data.source === C.OTP_SOURCES.SMS ? data.to : undefined,
                    emailId: data.source === C.OTP_SOURCES.MAIL ? data.to : undefined,
                };
                const success = ResponseFormatter.getSuccessResponse(200, MSG.OTP_VERIFIED, finalResponse);
                return Promise.resolve(success);
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC005", MSG.OTP_VERIFICATION_FAILED, MSG.OTP_VERIFICATION_FAILED, MSG.OTP_VERIFICATION_FAILED);
                return Promise.reject(error);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC006", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async uploadFileData(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC007", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            let inputFileUpload: any = req.files;
            if (Utils.isEmpty(inputFileUpload)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC008", MSG.FILE_UPLOADING_FAILED, MSG.FILE_UPLOADING_FAILED, MSG.FILE_UPLOADING_FAILED);
                return Promise.reject(error);
            }

            const urls = [];
            if (Array.isArray(inputFileUpload.file)) {
                let count = 0;
                for (const fileUpload of inputFileUpload.file) {
                    const fileExtension = fileUpload.name.split(".").pop();
                    const datetime = moment().format("YYYYMMDDHHmmssSSS");
                    const fileName = req.body.id + "_" + datetime + "_" + ++count + "." + fileExtension;
                    const inputFile = __dirname + "/../../../resources/" + req.body.sourceType + "/" + req.body.fileType + "/" + fileName;
                    await fileUpload.mv(inputFile);
                    urls.push(req.protocol + "://" + req.headers.host + "/" + req.body.sourceType + "/" + req.body.fileType + "/" + fileName);
                }
            } else {
                inputFileUpload = inputFileUpload.file;
                const fileExtension = inputFileUpload.name.split(".").pop();
                const datetime = moment().format("YYYYMMDDHHmmssSSS");
                const fileName = req.body.id + "_" + datetime + "_1" + "." + fileExtension;
                const inputFile = __dirname + "/../../../resources/" + req.body.sourceType + "/" + req.body.fileType + "/" + fileName;
                await inputFileUpload.mv(inputFile);
                urls.push(req.protocol + "://" + req.headers.host + "/" + req.body.sourceType + "/" + req.body.fileType + "/" + fileName);
            }

            const updateData: any = {};
            let data: any = "";
            if (!Utils.isEmpty(req.body.sourceType) && req.body.sourceType === "category") {
                if (!Utils.isEmpty(req.body.fileType) && req.body.fileType === "image") {
                    updateData.image = urls[0];
                    data = await CategoryModel.findByIdAndUpdate(Types.ObjectId(req.body.id), updateData, { new: true });
                }
            } else if (!Utils.isEmpty(req.body.sourceType) && req.body.sourceType === "userProfile") {
                if (!Utils.isEmpty(req.body.fileType) && req.body.fileType === "image") {
                    updateData.image = urls[0];
                    data = await UserModel.findByIdAndUpdate(Types.ObjectId(req.body.id), updateData, { new: true });
                }
            } else if (!Utils.isEmpty(req.body.sourceType) && req.body.sourceType === "sellerServices") {
                const fields: any = { imageList: 1, videoList: 1, pdfList: 1, status: 1 };
                const SellerServices: any = await SellerServicesModel.findById(Types.ObjectId(req.body.id), fields);

                if (!Utils.isEmpty(SellerServices) && SellerServices.status === true) {
                    if (!Utils.isEmpty(req.body.fileType) && req.body.fileType === "image") {
                        SellerServices.imageList = urls;
                        updateData.imageList = SellerServices.imageList;
                    } else if (!Utils.isEmpty(req.body.fileType) && req.body.fileType === "video") {
                        SellerServices.videoList = urls;
                        updateData.videoList = SellerServices.videoList;
                    } else if (!Utils.isEmpty(req.body.fileType) && req.body.fileType === "pdf") {
                        SellerServices.pdfList = urls;
                        updateData.pdfList = SellerServices.pdfList;
                    }
                    data = await SellerServicesModel.findByIdAndUpdate(Types.ObjectId(req.body.id), updateData, { new: true });
                }
            }

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC009", MSG.FILE_UPLOADING_FAILED, MSG.FILE_UPLOADING_FAILED, MSG.FILE_UPLOADING_FAILED);
                return Promise.reject(error);
            } else {
                let resultUrls = [];
                if (!Utils.isEmpty(data.image)) {
                    resultUrls.push(data.image);
                } else if (!Utils.isEmpty(data.imageList)) {
                    resultUrls = data.imageList;
                } else if (!Utils.isEmpty(data.videoList)) {
                    resultUrls = data.videoList;
                } else if (!Utils.isEmpty(data.pdfList)) {
                    resultUrls = data.pdfList;
                }
                const fileData = {
                    urls: resultUrls,
                };
                const success = ResponseFormatter.getSuccessResponse(200, MSG.FILE_UPLOADED_SUCCESSFULLY, fileData);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC010", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getConfigData(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC011", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const condition: any = { status: true };
            if (!Utils.isEmpty(req.params.configName)) {
                condition.configName = req.params.configName;
            }
            const fields: any = { configName: 1, configValue: 1, _id: 0 };
            const data = await ConfigModel.find(condition, fields);
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC012", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC013", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async sendNotification(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC014", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = { userType: 1, notificationToken: 1 };
            const condition: any = {
                _id: { $in: req.body.userIds },
                notificationToken: { $ne: null },
                status: true,
            };
            const userData = await UserModel.find(condition, fields);
            const data: any = await NotificationService.sendNotification(userData, req.body.notificationData);
            if (!Utils.isEmpty(data.status)) {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.NOTIFICATION_SEND);
                return Promise.resolve(success);
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC015", MSG.NOTIFICATION_SENDING_FAILED, data.response, MSG.NOTIFICATION_SENDING_FAILED);
                return Promise.reject(error);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC016", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    // public async updateNotificationSeenStatus(req: Request, res: Response) {

    //     try {

    //         const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
    //         if (Utils.isEmpty(authorizationResult)) {
    //             const error = ResponseFormatter.getErrorResponseWithBody(500, "CC017", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
    //             return Promise.reject(error);
    //         }

    //         const fields: any = { userType: 1, notificationToken: 1 };
    //         const condition: any = {
    //             _id: { $in: req.body.userIds },
    //             notificationToken: { $ne: null },
    //             status: true,
    //         };
    //         const userData = await UserModel.find(condition, fields);
    //         const data: any = await NotificationService.sendNotification(userData, req.body.notificationData);
    //         if (!Utils.isEmpty(data.status)) {
    //             const success = ResponseFormatter.getSuccessResponse(200, MSG.NOTIFICATION_SEND);
    //             return Promise.resolve(success);
    //         } else {
    //             const error = ResponseFormatter.getErrorResponseWithBody(500, "CC018", MSG.NOTIFICATION_SENDING_FAILED, data.response, MSG.NOTIFICATION_SENDING_FAILED);
    //             return Promise.reject(error);
    //         }
    //     } catch (Exception) {

    //         console.log(Exception);
    //         const error = ResponseFormatter.getErrorResponseWithBody(500, "CC019", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
    //         return Promise.reject(error);
    //     }
    // }

    public async getNotificationHistory(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC020", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = "body isSeen createdAt";
            const condition: any = {
                userMappingKey: req.body.userId,
                sendStatus: true,
            };

            const options = {
                select: fields,
                sort: { createdAt: -1 },
                lean: true,
                leanWithId: false,
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
            };
            const data: any = await NotificationModel.paginate(condition, options);

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC021", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                for (const notifiationData of data.docs) {
                    notifiationData.createdAt = Utils.convertDateTimeFormatFromDB(notifiationData.createdAt);
                }
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC022", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async sendEmailIdVerificationMail(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC023", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const user: any = await UserModel.findOne({ token: req.headers.authorization }, { userType: 1 });
            const params = {
                emailId: req.body.source,
                userId: user.id,
                userType: user.userType,
            };

            const htmlFile = __dirname + "/../../../views/emailTemplate/mailIdVerification.ejs";
            const compiled = ejs.compile(fs.readFileSync(htmlFile, "utf8"));
            const mailBody = compiled(params);
            const mailSubject = "Please Verify Your Mail ID";

            if (req.body.source.includes("@")) {
                const condition: any = { status: true, configName: "mailConfig" };
                const fields: any = { configValue: 1, _id: 0 };
                let configData: any = await ConfigModel.findOne(condition, fields);
                if (!Utils.isEmpty(configData)) {
                    configData = configData.configValue[0];
                    const data: any = await SmsAndMailService.sendMail(mailSubject, mailBody, req.body.source, configData);

                    if (!Utils.isEmpty(data.status)) {
                        const success = ResponseFormatter.getSuccessResponse(200, MSG.MAIL_SEND);
                        return Promise.resolve(success);
                    } else {
                        const error = ResponseFormatter.getErrorResponseWithBody(500, "CC024", MSG.MAIL_SENDING_FAILED, data.response, MSG.MAIL_SENDING_FAILED);
                        return Promise.reject(error);
                    }
                } else {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "CC025", MSG.MAIL_SENDING_FAILED, MSG.MAIL_SENDING_FAILED, MSG.MAIL_SENDING_FAILED);
                    return Promise.reject(error);
                }
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC026", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC027", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async verifyEmailId(req: Request, res: Response) {

        try {
            const updateData: any = {
                isEmailIdVerified: true,
            };
            const data = await UserModel.findByIdAndUpdate(req.params.userId, updateData, { new: true });

            if (!Utils.isEmpty(data)) {
                const htmlFile = __dirname + "/../../../views/emailTemplate/mailIdVerified.ejs";
                const compiled = ejs.compile(fs.readFileSync(htmlFile, "utf8"));
                const htmlString = compiled();
                res.send(htmlString);
                const success = ResponseFormatter.getSuccessResponse(200, MSG.MAIL_VERIFIED);
                return Promise.resolve(success);
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC028", MSG.OTP_VERIFICATION_FAILED, MSG.OTP_VERIFICATION_FAILED, MSG.OTP_VERIFICATION_FAILED);
                return Promise.reject(error);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC029", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async createOffer(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC030", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const offerDataObj: any = {};
            if (!Utils.isEmpty(req.body.title)) {
                offerDataObj.title = req.body.title;
            }

            if (!Utils.isEmpty(req.body.discountPercentage)) {
                offerDataObj.discountPercentage = req.body.discountPercentage;
            }

            if (!Utils.isEmpty(req.body.validFrom)) {
                offerDataObj.validFrom = Utils.getDateTimeFormatForDB(req.body.validFrom);
            }

            if (!Utils.isEmpty(req.body.validTo)) {
                offerDataObj.validTo = Utils.getDateTimeFormatForDB(req.body.validTo);
            }

            if (Utils.isSet(req.body.status)) {
                offerDataObj.status = (/true/i).test(req.body.status);
            }

            const offerModel = new OffersModel(offerDataObj);
            const data = await offerModel.save();

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC031", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC032", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async updatepOfferDetails(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC033", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const offerDataObj: any = {};
            if (!Utils.isEmpty(req.body.title)) {
                offerDataObj.title = req.body.title;
            }

            if (!Utils.isEmpty(req.body.discountPercentage)) {
                offerDataObj.discountPercentage = req.body.discountPercentage;
            }

            if (!Utils.isEmpty(req.body.validFrom)) {
                offerDataObj.validFrom = Utils.getDateTimeFormatForDB(req.body.validFrom);
            }

            if (!Utils.isEmpty(req.body.validTo)) {
                offerDataObj.validTo = Utils.getDateTimeFormatForDB(req.body.validTo);
            }

            if (Utils.isSet(req.body.status)) {
                offerDataObj.status = (/true/i).test(req.body.status);
            }

            const data = await OffersModel.findByIdAndUpdate(req.params._id, offerDataObj, { new: true });

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC034", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC035", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async updatepOfferMapping(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC036", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            if (Utils.isEmpty(req.body.sellerServiceIds)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC037", MSG.INVALID_REQUEST, MSG.INVALID_REQUEST, MSG.INVALID_REQUEST);
                return Promise.reject(error);
            }

            const promiseArray: any = [];
            const fields: any = { _id: 1, offers: 1 };
            const condition: any = { status: true };

            if (!Utils.isEmpty(req.body.categoryId)) {
                condition.category = req.body.categoryId;
            }

            if (!Utils.isEmpty(req.body.subCategoryId)) {
                condition.subCategory = req.body.subCategoryId;
            }

            if (!Utils.isEmpty(req.body.sellerId)) {
                condition.userMappingKey = req.body.sellerId;
            }

            condition.offers = { $ne: req.body.offerId };
            condition._id = { $in: req.body.sellerServiceIds };
            promiseArray.push(SellerServicesModel.find(condition, fields));

            condition.offers = req.body.offerId;
            condition._id = { $nin: req.body.sellerServiceIds };
            promiseArray.push(SellerServicesModel.find(condition, fields));

            const results: any = await Promise.all(promiseArray);
            const servicesNeedToMap: any = results[0];
            const servicesNeedToUnMap: any = results[1];

            if (!Utils.isEmpty(servicesNeedToMap)) {
                for (const service of servicesNeedToMap) {
                    service.offers.push(Types.ObjectId(req.body.offerId));
                    await SellerServicesModel.findByIdAndUpdate(service._id, service, { new: true });
                }
            }

            if (!Utils.isEmpty(servicesNeedToUnMap)) {
                for (const service of servicesNeedToUnMap) {
                    const index = service.offers.indexOf(Types.ObjectId(req.body.offerId));
                    if (index > -1) {
                        service.offers.splice(index, 1);
                    }
                    await SellerServicesModel.findByIdAndUpdate(service._id, service, { new: true });
                }
            }

            const success = ResponseFormatter.getSuccessResponse(200, MSG.RECORD_UPDATED_SUCCESSFULLY);
            return Promise.resolve(success);

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC038", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getOffersList(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC039", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = "title discountPercentage validFrom validTo status";
            const condition: any = {};

            if (!Utils.isEmpty(req.body.searchParam)) {
                condition.title = { $regex: new RegExp(req.body.searchParam, "i") };
            }

            const options = {
                select: fields,
                sort: { createdAt: -1 },
                lean: true,
                leanWithId: false,
                offset: Utils.isEmpty(req.body.skip) ? 0 : req.body.skip,
                limit: Utils.isEmpty(req.body.limit) ? 10 : req.body.limit,
            };
            const data: any = await OffersModel.paginate(condition, options);

            if (Utils.isEmpty(data.total)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC040", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                for (const offerData of data.docs) {
                    offerData.validFrom = Utils.convertDateTimeFormatFromDB(offerData.validFrom);
                    offerData.validTo = Utils.convertDateTimeFormatFromDB(offerData.validTo);
                }
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC041", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getOfferDetails(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC042", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = "title discountPercentage validFrom validTo status";
            const data: any = await OffersModel.findById(Types.ObjectId(req.body.offerId), fields).lean();

            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CC043", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                data.validFrom = Utils.convertDateTimeFormatFromDB(data.validFrom);
                data.validTo = Utils.convertDateTimeFormatFromDB(data.validTo);

                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC044", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    private async generateOtp(sourceType: string, source: string): Promise<string> {
        try {
            const previousDateTime = moment().add(-15, "minutes").toDate();
            const currentDateTime = moment().toDate();
            const condition: any = {
                source: sourceType,
                to: source,
                createdAt: { $gte: previousDateTime, $lte: currentDateTime },
            };
            const fields: any = { _id: 0, otp: 1 };
            const sortBy: any = { sort: { _id: -1 } };
            const SmsAndMailData: any = await SmsAndMailModel.findOne(condition, fields, sortBy);

            let otpNumber: string = "";
            if (Utils.isEmpty(SmsAndMailData)) {
                const otpLength = 6;
                const minimum = Number(("9").repeat((otpLength - 1)));
                const maximum = Number(("9").repeat((otpLength)));
                otpNumber = (Math.floor(Math.random() * (maximum - minimum + 1)) + minimum).toString();
            } else {
                otpNumber = SmsAndMailData.otp;
            }

            return Promise.resolve(otpNumber);
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "CC045", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }
}
