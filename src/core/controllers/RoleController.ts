import { Request, Response, Router } from "express";

import { Utils } from "../../lib/Utils";
import { C } from "../../config/constants/constants";
import { JwtTokenService } from "../services/JwtTokenService";
import moment = require("moment-timezone");
moment.tz.setDefault(C.TIMEZONE);
import { ResponseFormatter } from "../../lib/ResponseFormatter";
import { MessagesConstants as MSG } from "../../config/constants/messages.constants";

import { RoleModel } from "../models/RoleModel";

export class RoleController {

    constructor() {

    }

    public async addRole(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "CATC009", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const roleKey = ((req.body.role).toLowerCase()).replace(/\s\s+/g, " ");
            const role = new RoleModel({
                role: req.body.role,
                roleKey: roleKey.replace(/ /g, "-"),
                sourceId: Utils.isSet(req.body.sourceId) ? req.body.sourceId : true,
                createdAt: new Date(),
                updatedAt: new Date(),
            });

            const data = await role.save();
            const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
            return Promise.resolve(success);
        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC002", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async editRole(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC006", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const updateData: any = {};

            if (!Utils.isEmpty(req.body.role)) {
                updateData.role = req.body.role;
            }

            if (!Utils.isEmpty(req.body.sourceId)) {
                updateData.sourceId = req.body.sourceId;
            }

            if (!Utils.isEmpty(req.body.status)) {
                updateData.status = req.body.status;
            }

            const data = await RoleModel.findByIdAndUpdate(req.params._id, updateData, { new: true });
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC007", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC008", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async disableRole(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC006", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const updateData: any = { status: false };

            const data = await RoleModel.findByIdAndUpdate(req.params._id, updateData, { new: true });
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC007", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC008", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async enableRole(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC006", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const updateData: any = { status: true };

            const data = await RoleModel.findByIdAndUpdate(req.params._id, updateData, { new: true });
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC007", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC008", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getRoleDetail(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC009", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = { role: 1, roleKey: 1, sourceId: 1, status: 1 };
            const condition: any = {};
            if (!Utils.isEmpty(req.params._id)) {
                condition._id = req.params._id;
            }
            const data = await RoleModel.find(condition, fields);
            if (Utils.isEmpty(data)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC010", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            } else {
                const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                return Promise.resolve(success);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC011", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async grantPermission(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC009", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            // TODO also check if valid role and path/method
            // TODO update acl
            const data = await C.aclObj.allow(req.body.roleKey, req.body.path, req.body.method);
            const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
            return Promise.resolve(success);

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC011", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async revokePermission(req: Request, res: Response) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC009", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            // TODO also check if valid role and path/method
            // TODO update acl
            const data = await C.aclObj.removeAllow(req.body.roleKey, req.body.path, req.body.method);
            const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
            return Promise.resolve(success);

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC011", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    public async getRolePermissions(req: Request, res: Response, app: Router) {

        try {

            const authorizationResult = await JwtTokenService.verifyToken(req.headers.authorization);
            if (Utils.isEmpty(authorizationResult)) {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC009", MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR, MSG.AUTHENTICATION_ERROR);
                return Promise.reject(error);
            }

            const fields: any = { role: 1, roleKey: 1, sourceId: 1, status: 1 };
            const roleData: any = await RoleModel.findById(req.params._id, fields);
            if (!Utils.isEmpty(roleData)) {
                const allRoutes = this.getAllRoutes(req, res, app);
                const allowedPermissions = await C.aclObj.whatResources(roleData.roleKey);
                if (!allowedPermissions) {
                    const error = ResponseFormatter.getErrorResponseWithBody(500, "UC010", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                    return Promise.reject(error);
                } else {
                    const limit = req.body.limit;
                    const skip = !Utils.isEmpty(req.body.skip) ? req.body.skip : 0;
                    const routesData: any = [];
                    for (let index = 0; index < allRoutes.length; index++) {
                        if (index < skip) {
                            continue;
                        }
                        if (index < (skip + limit)) {
                            routesData.push(allRoutes[index]);
                        }
                    }

                    const data = {
                        allRoutes: routesData,
                        allowedPermissions,
                        role: roleData,
                        total: allRoutes.length,
                    };
                    const success = ResponseFormatter.getSuccessResponse(200, MSG.BLANK_MSG, data);
                    return Promise.resolve(success);
                }
            } else {
                const error = ResponseFormatter.getErrorResponseWithBody(500, "UC010", MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND, MSG.NO_RECORDS_FOUND);
                return Promise.reject(error);
            }

        } catch (Exception) {

            console.log(Exception);
            const error = ResponseFormatter.getErrorResponseWithBody(500, "UC011", MSG.SOMETHING_WENT_WRONG, Exception.message, MSG.SOMETHING_WENT_WRONG);
            return Promise.reject(error);
        }
    }

    private getAllRoutes(req: Request, res: Response, app: any) {

        const routes: any = [];
        let route: any = [];
        const routesData: any = [];
        app._router.stack.forEach((middleware: any) => {
            if (middleware.route) { // routes registered directly on the app
                routes.push(middleware.route);
            } else if (middleware.name === "router") { // router middleware
                middleware.handle.stack.forEach((handler: any) => {
                    route = handler.route;
                    routes.push(route);
                });
            }
        });

        routes.forEach((temp: any, index: number) => {
            let methods = "";
            if (!Utils.isEmpty(temp)) {
                const tempMethods = !Utils.isEmpty(temp.methods) ? temp.methods : [];
                for (const method in tempMethods) {
                    if (!Utils.isEmpty(method)) {
                        methods += method + ", ";
                    }
                }
                methods = methods.substr(0, methods.length - 2);
                if (!Utils.isEmpty(req.body.path) && (temp.path).includes(req.body.path)) {
                    routesData.push({
                        path: temp.path,
                        method: methods,
                    });
                } else if (Utils.isEmpty(req.body.path)) {
                    routesData.push({
                        path: temp.path,
                        method: methods,
                    });
                }
            }
        });
        return routesData;
    }
}
