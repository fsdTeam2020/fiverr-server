// const format = require('string-format');
// import { format } from 'string-format';
import { NextFunction, Request, Response } from "express";
import { C } from "../config/constants/constants";
import { IErrorResponseBody, IErrorResponseBodyOptionalParams, IErrorResponseFormat, ISuccessResponseFormat } from "./IResponseFormat";
import { Utils } from "./Utils";

export class ResponseFormatter {

    public static getSuccessResponse(httpSuccessCode: number, displayMessage: string, responseObject: any = {}): ISuccessResponseFormat {

        const returnSuccessObject: ISuccessResponseFormat = {
            status: 200, displayMessage: "", body: {},
        };

        returnSuccessObject.status = httpSuccessCode;
        returnSuccessObject.displayMessage = displayMessage;
        returnSuccessObject.body = responseObject;

        return returnSuccessObject;
    }

    /**
     *
     * @param code
     * @param message
     * @param detail
     * @param displayMessage
     * @param options
     */
    public static getErrorResponseBody(code: string, message: string, detail: string = "", displayMessage: string = "", options: IErrorResponseBodyOptionalParams = {}): IErrorResponseBody {

        const errorObject: IErrorResponseBody = {
            code: "", message: "", detail: "", displayMessage: "",
        };

        errorObject.code = code;
        errorObject.message = message;
        errorObject.detail = detail ? detail : message;
        errorObject.displayMessage = displayMessage ? displayMessage : message;
        errorObject.errCategory = "";

        if (Utils.isSet(options.errCategory)) {
            errorObject.errCategory = options.errCategory;
        }
        if (Utils.isSet(options.planId)) {
            errorObject.planId = options.planId;
        }
        if (Utils.isSet(options.proposalStatus)) {
            errorObject.proposalStatus = options.proposalStatus;
        }

        // cl(errorObject,__line,__file);
        return errorObject;
    }

    /**
     * @returns IErrorResponseFormat
     * @param httpErrorCode number
     * @param errorObject IErrorResponseBody
     */
    public static getErrorResponse(httpErrorCode: number, errorObject: IErrorResponseBody): IErrorResponseFormat {

        const returnErrorObject: IErrorResponseFormat = {
            status: 0, body: [],
        };
        let errorArray: IErrorResponseBody[] = [];

        if (Array.isArray(errorObject)) {
            errorArray = errorObject;
        } else {
            errorArray.push(errorObject);
        }

        returnErrorObject.status = httpErrorCode;
        returnErrorObject.body = errorArray;

        return returnErrorObject;
    }

    /**
     * @param httpErrorCode
     * @param code
     * @param message
     * @param detail
     * @param displayMessage
     * @param options
     */
    public static getErrorResponseWithBody(httpErrorCode: number, code: string, message: string, detail: string = "", displayMessage: string = "", options: IErrorResponseBodyOptionalParams = {}): IErrorResponseFormat {

        const errorObject = this.getErrorResponseBody(code, message, detail, displayMessage, options);
        return this.getErrorResponse(httpErrorCode, errorObject);
    }

    /**
     * Format joi's error msg to our standard error msg format
     * @param errors
     * @param errCode
     */
    public static formatValidationErrorMsg(errors: any, errCode: string) {

        const errorObject: any = [];
        if (!Utils.isEmpty(errors)) {

            console.log("errors:-", errors);

            let errorMsg = "";
            errors.forEach((element: any, counter: number) => {

                errorMsg += element.message + ", ";

                const errCodeT = errCode + (counter + 1);
                errorMsg = errorMsg.slice(0, -2);
                errorObject.push(this.getErrorResponseBody(errCodeT, errorMsg, "", errorMsg));
            });
        }

        const finalErr = this.getErrorResponse(400, errorObject);
        console.log("finalErr:-", finalErr);
        return finalErr;
    }
    constructor() {

    }
}
