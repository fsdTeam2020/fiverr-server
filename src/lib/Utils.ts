import { Request, Response, Router } from "express";
import moment = require("moment-timezone");
moment.tz.setDefault("Asia/Calcutta");
import { C } from "../config/constants/constants";

export class Utils {

    public static capitalize(str: string): string {
        return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
    }

    public static getArrayCopy(array: any): any {
        return JSON.parse(JSON.stringify(array));
    }

    /**
     * checks if a data type is undefined or not
     * @param data
     * @returns {boolean}
     */
    public static isSet(data: any): boolean {
        return typeof data !== "undefined";
    }

    /**
     * checks if a data type is undefined or not, works nested too
     * @param obj
     * @returns {boolean}
     */
    public static isSetNested(obj: any /*, level1, level2, ... levelN*/): boolean {

        const args = Array.prototype.slice.call(arguments, 1);

        // for (let i = 0; i < args.length; i++) {
        for (let i of args) {
            if (!obj || !this.isSet(obj[args[i]])) {
                return false;
            }
            obj = obj[args[i]];
            i++;
        }

        return true;
    }

    /**
     * returns true if data is '' or null or undefined
     * for object, checks if it's length is non zero or not
     * @param data {Mixed}
     * @param zeroIsNotEmpty {boolean}: pass true only if you want 0 to be considered as non-empty
     * @returns {boolean}
     */
    public static isEmpty(data: any, zeroIsNotEmpty: boolean = false): boolean {

        if (typeof data !== "object" && (data === null || data === "" || typeof data === "undefined")) {
            return true;
        } else if (data === null) {
            return true;
        } else if (typeof data === "string" && data === "0" && !zeroIsNotEmpty) {
            return true;
        } else if (typeof data.length !== "undefined") {
            if (data.length > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            if (Object.keys(data).length > 0) {
                return false;
            } else if (typeof data === "number" && (data !== 0 || zeroIsNotEmpty)) {
                return false;
            } else {
                if (data === true) {
                    return false;
                }
                return true;
            }
        }
    }

    public static getDateFormatForDB(date: Date, currentFormat: string = C.DATE_FORMAT): Date {
        currentFormat = this.isEmpty(currentFormat) ? C.DATE_FORMAT : currentFormat;
        return moment(date, currentFormat).toDate();
    }

    public static getDateTimeFormatForDB(date: Date, currentFormat: string = C.DATE_TIME_FORMAT): Date {
        currentFormat = this.isEmpty(currentFormat) ? C.DATE_TIME_FORMAT : currentFormat;
        return moment(date, currentFormat).toDate();
    }

    public static convertDateFormatFromDB(date: Date, requiredFormat: string = C.DATE_FORMAT): string {
        requiredFormat = this.isEmpty(requiredFormat) ? C.DATE_FORMAT : requiredFormat;
        return moment(date).format(requiredFormat);
    }

    public static convertDateTimeFormatFromDB(date: Date, requiredFormat: string = C.DATE_TIME_FORMAT): string {
        requiredFormat = this.isEmpty(requiredFormat) ? C.DATE_TIME_FORMAT : requiredFormat;
        return moment(date).format(requiredFormat);
    }

    public static changeDateFormat(date: Date, newFormat: string, oldFormat: string = C.DATE_FORMAT): string {
        oldFormat = this.isEmpty(oldFormat) ?  C.DATE_FORMAT : oldFormat;
        return moment(date, oldFormat).format(newFormat);
    }

    public static changeDateTimeFormat(dateTime: Date, newFormat: string, oldFormat: string = C.DATE_TIME_FORMAT): string {
        oldFormat = this.isEmpty(oldFormat) ? C.DATE_TIME_FORMAT : oldFormat;
        return moment(dateTime, oldFormat).format(newFormat);
    }

    constructor() {

    }
}
